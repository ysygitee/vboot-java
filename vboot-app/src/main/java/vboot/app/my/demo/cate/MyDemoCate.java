package vboot.app.my.demo.cate;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import vboot.core.common.mvc.entity.BaseCateEntity;

import javax.persistence.*;

@Entity
@Getter
@Setter
@ApiModel("DEMO分类")
public class MyDemoCate extends BaseCateEntity {

    @ManyToOne(fetch= FetchType.EAGER)
    @JoinColumn(name = "pid")
    @ApiModelProperty("父类别")
    private MyDemoCate parent;

}
