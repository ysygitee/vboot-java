package vboot.app.my.demo.cate;

import org.springframework.data.jpa.repository.JpaRepository;

//DEMO分类JPA仓储
public interface MyDemoCateRepo extends JpaRepository<MyDemoCate,String> {


}
