package vboot.app.my.demo.cate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Service;
import vboot.core.common.mvc.dao.Sqler;
import vboot.core.common.mvc.service.BaseCateService;

import javax.annotation.PostConstruct;
import java.util.List;

//DEMO分类服务
@Service
public class MyDemoCateService extends BaseCateService<MyDemoCate> {

    //获取分类treeTable数据
    public List<MyDemoCate> findTree(Sqler sqler) {
        List<MyDemoCate> list = jdbcDao.getTp().query(sqler.getSql(), sqler.getParams(),
                new BeanPropertyRowMapper<>(MyDemoCate.class));
        return buildByRecursive(list);
    }

    @Autowired
    private MyDemoCateRepo repo;

    @PostConstruct
    public void initDao() {
        super.setRepo(repo);
    }

}
