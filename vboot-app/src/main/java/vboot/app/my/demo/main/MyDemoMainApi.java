package vboot.app.my.demo.main;

import cn.hutool.core.lang.Dict;
import com.dingtalk.api.DefaultDingTalkClient;
import com.dingtalk.api.DingTalkClient;
import com.dingtalk.api.request.OapiGettokenRequest;
import com.dingtalk.api.response.OapiGettokenResponse;
import com.taobao.api.ApiException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import vboot.core.common.mvc.api.R;
import vboot.core.common.mvc.dao.Sqler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import vboot.core.common.utils.ruoyi.JsonUtils;
import vboot.core.module.ass.num.main.AssNumMainService;
import vboot.core.module.mon.log.oper.Oplog;

@RestController
@RequestMapping("my/demo/main")
@Api(tags = {"DEMO管理"})
public class MyDemoMainApi {

    @Oplog("查询DEMO分页")
    @GetMapping
    @ApiOperation("查询DEMO分页")
    public R get(String name) {
        Sqler sqler = new Sqler("my_demo_main");
        sqler.addLike("t.name", name);
        sqler.addSelect("t.addre,t.senum,t.notes");
        return R.ok(service.findPageData(sqler));
    }

    @Oplog("查询DEMO详情")
    @GetMapping("one/{id}")
    @ApiOperation("查询DEMO详情")
    public R getOne(@PathVariable String id) throws ApiException {
        MyDemoMain main = service.findOne(id);
        return R.ok(main);
    }

    @Oplog("新增DEMO")
    @PostMapping
    @ApiOperation("新增DEMO")
    public R post(@RequestBody MyDemoMain main) {
        main.setSenum(numService.getNum("DEMO"));//设置供应商流水号
        return R.ok(service.insert(main));
    }

    @Oplog("更新DEMO")
    @PutMapping
    @ApiOperation("更新DEMO")
    public R put(@RequestBody MyDemoMain main) {
        return R.ok(service.update(main));
    }

    @Oplog("删除DEMO")
    @DeleteMapping("{ids}")
    @ApiOperation("删除DEMO")
    public R delete(@PathVariable String[] ids) {
        return R.ok(service.delete(ids));
    }

    @Autowired
    private MyDemoMainService service;

    @Autowired
    private AssNumMainService numService;

}
