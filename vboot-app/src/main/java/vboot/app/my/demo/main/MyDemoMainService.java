package vboot.app.my.demo.main;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vboot.core.common.mvc.service.BaseMainService;

import javax.annotation.PostConstruct;

//DEMO主数据服务
@Service
public class MyDemoMainService extends BaseMainService<MyDemoMain> {

    @Autowired
    private MyDemoMainRepo repo;

    @PostConstruct
    public void initDao() {
        super.setRepo(repo);
    }

}

