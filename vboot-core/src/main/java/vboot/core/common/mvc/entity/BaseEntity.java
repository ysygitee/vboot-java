package vboot.core.common.mvc.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

//简单Entity基类，仅提供id与name字段，如有租户需求，可加租户ID。
@MappedSuperclass
@Getter
@Setter
public class BaseEntity {

    @Id
    @Column(length = 32)
    @ApiModelProperty("主键ID")
    protected String id;

    @Column(length = 126)
    @ApiModelProperty("名称")
    protected String name;

}
