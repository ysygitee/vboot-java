package vboot.core.common.mvc.vo;


import java.util.List;

public class BaseTreeVo {

    private String id;

    private String name;

    private String pid;

    private List<BaseTreeVo> children;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public List<BaseTreeVo> getChildren() {
        return children;
    }

    public void setChildren(List<BaseTreeVo> children) {
        this.children = children;
    }
}
