package vboot.core.common.mvc.vo;

import java.util.ArrayList;
import java.util.List;

public class BaseTreeVoHand<T extends BaseTreeVo> {

    public  List<T> buildTreeByRecursive(List<T> nodes) {
        List<T> list = new ArrayList<>();
        for (T node : nodes) {
            if (node.getPid() == null) {
                list.add(findTreeChildrenByTier(node, nodes));
            } else {
                boolean flag = false;
                for (T node2 : nodes) {
                    if (node.getPid().equals(node2.getId())) {
                        flag = true;
                        break;
                    }
                }
                if (!flag) {
                    list.add(findTreeChildrenByTier(node, nodes));
                }
            }
        }
        return list;
    }

    //递归查找子节点
    private T findTreeChildrenByTier(T node, List<T> nodes) {
        for (T item : nodes) {
            if (node.getId().equals(item.getPid())) {
                if (node.getChildren() == null) {
                    node.setChildren(new ArrayList<>());
                }
                node.getChildren().add(findTreeChildrenByTier(item, nodes));
            }
        }
        return node;
    }

}
