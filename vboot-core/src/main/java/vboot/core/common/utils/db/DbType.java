package vboot.core.common.utils.db;

public class DbType {

    public static final String MYSQL = "mysql";

    public static final String MYSQL8 = "mysql8";

    public static final String ORACLE = "oracle";

    public static final String SQL_SERVER = "sqlserver";

    public static final String DB2 = "db2";

}
