/*
 *  Copyright 2019-2020 Zheng Jie
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package vboot.core.common.utils.db;

import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.druid.util.StringUtils;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jdbc.core.JdbcTemplate;
import vboot.core.common.utils.lang.XstrUtil;
import vboot.core.module.mon.server.main.CloseUtil;

import javax.sql.DataSource;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author /
 */
@Slf4j
public class SqlUtils {

    private static Map<String, DruidDataSource> dataSourceMap = new HashMap<>();

    private static Map<String, Jdao> jdaoMap = new HashMap<>();

    private static DataSource getDataSource(Zconn zconn) {
        if (XstrUtil.isNotBlank(zconn.getName())) {
            DruidDataSource ds = dataSourceMap.get(zconn.getName());
            if (ds != null) {
                return ds;
            }
        }
        DruidDataSource druidDataSource = new DruidDataSource();
        String className;
        try {
            className = DriverManager.getDriver(zconn.getUrl().trim()).getClass().getName();
        } catch (SQLException e) {
            throw new RuntimeException("Get class name error: =" + zconn.getUrl());
        }
        if (StringUtils.isEmpty(className)) {
            DataTypeEnum dataTypeEnum = DataTypeEnum.urlOf(zconn.getUrl());
            if (null == dataTypeEnum) {
                throw new RuntimeException("Not supported data type: jdbcUrl=" + zconn.getUrl());
            }
            druidDataSource.setDriverClassName(dataTypeEnum.getDriver());
        } else {
            druidDataSource.setDriverClassName(className);
        }

        if (XstrUtil.isNotBlank(zconn.getName())) {
            druidDataSource.setName(zconn.getName());
        }
        druidDataSource.setUrl(zconn.getUrl());
        druidDataSource.setUsername(zconn.getUsername());
        druidDataSource.setPassword(zconn.getPasscode());
        // 配置获取连接等待超时的时间
        druidDataSource.setMaxWait(3000);
        // 配置初始化大小、最小、最大
        druidDataSource.setInitialSize(1);
        druidDataSource.setMinIdle(1);
        druidDataSource.setMaxActive(5);

        if(DbType.SQL_SERVER.equals(zconn.getType())){
            druidDataSource.setValidationQuery("SELECT 1");
        }

        // 如果链接出现异常则直接判定为失败而不是一直重试
        druidDataSource.setBreakAfterAcquireFailure(true);
        try {
            druidDataSource.init();
        } catch (SQLException e) {
            log.error("Exception during pool initialization", e);
            throw new RuntimeException(e.getMessage());
        }
        if(XstrUtil.isNotBlank(zconn.getName())){
            dataSourceMap.put(zconn.getName(), druidDataSource);
        }
        return druidDataSource;
    }

    public static Jdao getJdao(Zconn zconn) {
        Jdao jdao = jdaoMap.get(zconn.getName());
        if (jdao != null) {
            return jdao;
        } else {
            DataSource druidDataSource = getDataSource(zconn);
            JdbcTemplate jdbcTemplate = new JdbcTemplate(druidDataSource);
            jdao = new Jdao(jdbcTemplate, zconn.getType());
            jdaoMap.put(zconn.getName(), jdao);
            return jdao;
        }
    }

    private static Connection getConnection(Zconn zconn) {
        DataSource dataSource = getDataSource(zconn);
        Connection connection = null;
        try {
            connection = dataSource.getConnection();
        } catch (Exception ignored) {
        }
        try {
            int timeOut = 5;
            if (null == connection || connection.isClosed() || !connection.isValid(timeOut)) {
                log.info("connection is closed or invalid, retry get connection!");
                connection = dataSource.getConnection();
            }
        } catch (Exception e) {
            log.error("create connection error, jdbcUrl: {}", zconn.getUrl());
            throw new RuntimeException("create connection error, jdbcUrl: " + zconn.getUrl());
        } finally {
            CloseUtil.close(connection);
        }
        return connection;
    }

    private static void releaseConnection(Connection connection) {
        if (null != connection) {
            try {
                connection.close();
            } catch (Exception e) {
                log.error(e.getMessage(), e);
                log.error("connection close error：" + e.getMessage());
            }
        }
    }

    public static boolean testConnection(Zconn zconn) {
        Connection connection = null;
        DataSource dataSource = null;
        try {
            dataSource = getDataSource(zconn);
            connection = dataSource.getConnection();
            int timeOut = 5;
            if (null == connection || connection.isClosed() || !connection.isValid(timeOut)) {
                log.info("connection is closed or invalid, retry get connection!");
                connection = dataSource.getConnection();
            }
            if (null != connection) {
                return true;
            }
        } catch (Exception e) {
            log.info("Get connection failed:" + e.getMessage());
        } finally {
            CloseUtil.close(connection);
            releaseConnection(connection);
            assert dataSource != null;
            ((DruidDataSource) dataSource).close();
        }
        return false;
    }

//	public static boolean testConnection(String name,String jdbcUrl, String userName, String password) {
//		Connection connection = null;
//		try {
//			connection = getConnection(name,jdbcUrl, userName, password);
//			if (null != connection) {
//				return true;
//			}
//		} catch (Exception e) {
//			log.info("Get connection failed:" + e.getMessage());
//		} finally {
//			releaseConnection(connection);
//		}
//		return false;
//	}

//    public static boolean execSql(String name, String jdbcUrl, String userName, String password) {
//        Connection connection = null;
//        try {
//            connection = getConnection(name, jdbcUrl, userName, password);
//            if (null != connection) {
//                return true;
//            }
//        } catch (Exception e) {
//            log.info("Get connection failed:" + e.getMessage());
//        } finally {
//            releaseConnection(connection);
//        }
//        return false;
//    }

//    public static String executeFile(String name, String jdbcUrl, String userName, String password, File sqlFile) {
//        Connection connection = getConnection(name, jdbcUrl, userName, password);
//        try {
//            batchExecute(connection, readSqlList(sqlFile));
//        } catch (Exception e) {
//            log.error("sql脚本执行发生异常:{}", e.getMessage());
//            return e.getMessage();
//        } finally {
//            releaseConnection(connection);
//        }
//        return "success";
//    }


    /**
     * 批量执行sql
     *
     * @param connection /
     * @param sqlList    /
     */
//    public static void batchExecute(Connection connection, List<String> sqlList) {
//        Statement st = null;
//        try {
//            st = connection.createStatement();
//            for (String sql : sqlList) {
//                if (sql.endsWith(";")) {
//                    sql = sql.substring(0, sql.length() - 1);
//                }
//                st.addBatch(sql);
//            }
//            st.executeBatch();
//        } catch (SQLException throwables) {
//            throwables.printStackTrace();
//        } finally {
//            CloseUtil.close(st);
//        }
//    }

    /**
     * 将文件中的sql语句以；为单位读取到列表中
     *
     * @param sqlFile /
     * @return /
     * @throws Exception e
     */
//    private static List<String> readSqlList(File sqlFile) throws Exception {
//        List<String> sqlList = Lists.newArrayList();
//        StringBuilder sb = new StringBuilder();
//        try (BufferedReader reader = new BufferedReader(new InputStreamReader(
//                new FileInputStream(sqlFile), StandardCharsets.UTF_8))) {
//            String tmp;
//            while ((tmp = reader.readLine()) != null) {
//                log.info("line:{}", tmp);
//                if (tmp.endsWith(";")) {
//                    sb.append(tmp);
//                    sqlList.add(sb.toString());
//                    sb.delete(0, sb.length());
//                } else {
//                    sb.append(tmp);
//                }
//            }
//            if (!"".endsWith(sb.toString().trim())) {
//                sqlList.add(sb.toString());
//            }
//        }
//
//        return sqlList;
//    }

}
