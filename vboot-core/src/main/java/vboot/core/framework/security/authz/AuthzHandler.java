package vboot.core.framework.security.authz;

import vboot.core.framework.security.pojo.Zuser;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

//权限校验处理器
@Component
public class AuthzHandler {

    public boolean hasPermission(HttpServletRequest request, Authentication authentication) {
        Zuser zuser;
        try {
            zuser = (Zuser) authentication.getPrincipal();
        } catch (Exception e) {
            //e.printStackTrace();
            return false;
        }
        String uri = request.getRequestURI();
        String method = request.getMethod();
        if("/getMenuList".equals(uri)||"/getUserInfo".equals(uri)){
            return true;
        }
        return checkPerm(zuser, method, uri);
    }

    private boolean checkPerm(Zuser zuser, String method,String url){
        if(zuser.isAdmin()){
            return true;
        }
        boolean flag=false;
        //根据method分类快速定位到请求url的权限码，再通过二进制&计算快速校验用户是否有权限
        if("GET".equals(method)){
            for (int i = 0; i < AuthzApiData.GET1_APIS.size(); i++) {
                if(AuthzApiData.GET1_APIS.get(i).getUrl().equals(url)){
                    flag = zuser.hasPerm(AuthzApiData.GET1_APIS.get(i).getPos(), AuthzApiData.GET1_APIS.get(i).getCode());
                    break;
                }
            }
            if(!flag){
                String frontUrl=url.substring(0,url.lastIndexOf("/"));
                for (int i = 0; i < AuthzApiData.GET2_APIS.size(); i++) {
                    if(AuthzApiData.GET2_APIS.get(i).getUrl().equals(frontUrl)){
                        flag = zuser.hasPerm(AuthzApiData.GET2_APIS.get(i).getPos(), AuthzApiData.GET2_APIS.get(i).getCode());
                        break;
                    }
                }
            }
        }else if("POST".equals(method)){
            for (int i = 0; i < AuthzApiData.POST_APIS.size(); i++) {
                if(AuthzApiData.POST_APIS.get(i).getUrl().equals(url)){
                    flag = zuser.hasPerm(AuthzApiData.POST_APIS.get(i).getPos(), AuthzApiData.POST_APIS.get(i).getCode());
                    break;
                }
            }
        }else if("PUT".equals(method)){
            for (int i = 0; i < AuthzApiData.PUT_APIS.size(); i++) {
                if(AuthzApiData.PUT_APIS.get(i).getUrl().equals(url)){
                    flag = zuser.hasPerm(AuthzApiData.PUT_APIS.get(i).getPos(), AuthzApiData.PUT_APIS.get(i).getCode());
                    break;
                }
            }
        }else if("DELETE".equals(method)){
            String frontUrl=url.substring(0,url.lastIndexOf("/"));
            for (int i = 0; i < AuthzApiData.DELETE_APIS.size(); i++) {
                if(AuthzApiData.DELETE_APIS.get(i).getUrl().equals(frontUrl)){
                    flag = zuser.hasPerm(AuthzApiData.DELETE_APIS.get(i).getPos(), AuthzApiData.DELETE_APIS.get(i).getCode());
                    break;
                }
            }
        }
        return flag;
    }

}
