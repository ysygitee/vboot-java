package vboot.core.framework.security.authz;

import lombok.RequiredArgsConstructor;
import vboot.core.framework.cache.RedisHandler;
import org.springframework.security.config.annotation.SecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.DefaultSecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

//授权过滤器适配器
@RequiredArgsConstructor
public class JwtFilterAdapter extends SecurityConfigurerAdapter<DefaultSecurityFilterChain, HttpSecurity> {

    private final JwtHandler jwtHandler;

    private final RedisHandler redisHandler;

    @Override
    public void configure(HttpSecurity http) {
        AuthzFilter jwtFilter = new AuthzFilter(jwtHandler, redisHandler);
        http.addFilterBefore(jwtFilter, UsernamePasswordAuthenticationFilter.class);
    }
}
