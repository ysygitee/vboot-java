package vboot.core.framework.security.pojo;

import lombok.Data;

import java.io.Serializable;

//api pojo:存储URL,权限位与权限码
@Data
public class Yapi implements Serializable {
    private static final long serialVersionUID = 7712491509249951764L;

    private int pos;

    private long code;

    private String url;

    //get and set------------
}
