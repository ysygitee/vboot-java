package vboot.core.framework.web;
import com.github.xiaoymin.knife4j.spring.annotations.EnableKnife4j;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import vboot.core.common.utils.ruoyi.StringUtils;
import vboot.core.common.utils.web.SpringUtils;

import javax.annotation.PostConstruct;

//Swagger 文档配置
@RequiredArgsConstructor
@Configuration
@EnableKnife4j
public class SwaggerConfig {

    @PostConstruct
    public void createRestApi() {
        Docket appDocket = new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(appInfo())
                .groupName("1.自建应用")
                .select()
                .apis(RequestHandlerSelectors.basePackage("vboot.app"))
                .paths(PathSelectors.any())
                .build();
        String app = StringUtils.substringAfterLast("vboot.app", ".") + "Docket";
        SpringUtils.registerBean(app, appDocket);

        Docket extDocket = new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(extInfo())
                .groupName("2.扩展应用")
                .select()
                .apis(RequestHandlerSelectors.basePackage("vboot.extend"))
                .paths(PathSelectors.any())
                .build();
        String ext = StringUtils.substringAfterLast("vboot.extend", ".") + "Docket";
        SpringUtils.registerBean(ext, extDocket);

        Docket sysDocket = new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(sysInfo())
                .groupName("3.系统管理")
                .select()
                .apis(RequestHandlerSelectors.basePackage("vboot.core.module.sys"))
                .paths(PathSelectors.any())
                .build();
        String sys = StringUtils.substringAfterLast("vboot.core.module.sys", ".") + "Docket";
        SpringUtils.registerBean(sys, sysDocket);

        Docket monDocket = new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(monInfo())
                .groupName("4.监控中心")
                .select()
                .apis(RequestHandlerSelectors.basePackage("vboot.core.module.mon"))
                .paths(PathSelectors.any())
                .build();
        String mon = StringUtils.substringAfterLast("vboot.core.module.mon", ".") + "Docket";
        SpringUtils.registerBean(mon, monDocket);

        Docket assDocket = new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(assInfo())
                .groupName("5.辅助工具")
                .select()
                .apis(RequestHandlerSelectors.basePackage("vboot.core.module.ass"))
                .paths(PathSelectors.any())
                .build();
        String ass = StringUtils.substringAfterLast("vboot.core.module.ass", ".") + "Docket";
        SpringUtils.registerBean(ass, assDocket);

        Docket genDocket = new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(genInfo())
                .groupName("6.通用模块")
                .select()
                .apis(RequestHandlerSelectors.basePackage("vboot.core.module.gen"))
                .paths(PathSelectors.any())
                .build();
        String gen = StringUtils.substringAfterLast("vboot.core.module.gen", ".") + "Docket";
        SpringUtils.registerBean(gen, genDocket);

        Docket bpmDocket = new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(bpmInfo())
                .groupName("7.流程引擎")
                .select()
                .apis(RequestHandlerSelectors.basePackage("vboot.core.module.bpm"))
                .paths(PathSelectors.any())
                .build();
        String bpm = StringUtils.substringAfterLast("vboot.core.module.bpm", ".") + "Docket";
        SpringUtils.registerBean(bpm, bpmDocket);

    }

    private ApiInfo appInfo() {
        return new ApiInfoBuilder()
                .title("自建应用")
                .description("这里是自定义的业务模块，在测试API请求之前，请先通过auth/login接口登录")
                .version("0.1")
                .build();
    }

    private ApiInfo extInfo() {
        return new ApiInfoBuilder()
                .title("扩展应用")
                .description("主要会有流程管理，数据库可视化，项目管理等")
                .version("0.1")
                .build();
    }

    private ApiInfo sysInfo() {
        return new ApiInfoBuilder()
                .title("系统管理")
                .description("系统管理主要包括组织架构，权限管理，一般只有系统管理员有此分组的权限")
                .version("0.1")
                .build();
    }

    private ApiInfo monInfo() {
        return new ApiInfoBuilder()
                .title("监控中心")
                .description("监控中心主要包括系统日志，定时任务，服务监控等，一般只有系统管理员有此分组的权限")
                .version("0.1")
                .build();
    }

    private ApiInfo assInfo() {
        return new ApiInfoBuilder()
                .title("辅助工具")
                .description("辅助模块主要有数据字典，代码生成器等，为业务模块服务")
                .version("0.1")
                .build();
    }

    private ApiInfo genInfo() {
        return new ApiInfoBuilder()
                .title("通用模块")
                .description("通用模块一般只要登录了，不管什么角色，都应有的通用公共权限")
                .version("0.1")
                .build();
    }

    private ApiInfo bpmInfo() {
        return new ApiInfoBuilder()
                .title("流程引擎")
                .description("流程引擎是针对流程的专门模块")
                .version("0.1")
                .build();
    }

}
