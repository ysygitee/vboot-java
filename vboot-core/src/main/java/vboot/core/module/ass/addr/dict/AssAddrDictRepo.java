package vboot.core.module.ass.addr.dict;

import org.springframework.data.jpa.repository.JpaRepository;

public interface AssAddrDictRepo extends JpaRepository<AssAddrDict, String> {

}