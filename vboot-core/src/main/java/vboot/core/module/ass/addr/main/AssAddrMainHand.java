package vboot.core.module.ass.addr.main;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import vboot.core.module.ass.addr.city.AssAddrCity;
import vboot.core.module.ass.addr.city.AssAddrCityRepo;
import vboot.core.module.ass.addr.dict.AssAddrDict;
import vboot.core.module.ass.addr.dict.AssAddrDictRepo;
import vboot.core.module.ass.addr.pojo.*;
import vboot.core.module.ass.addr.prov.AssAddrProv;
import vboot.core.module.ass.addr.prov.AssAddrProvRepo;
import vboot.core.module.ass.addr.stre.AssAddrStre;
import vboot.core.module.ass.addr.stre.AssAddrStreRepo;

import java.util.ArrayList;
import java.util.List;

@Component
public class AssAddrMainHand {

    public void addrInit() {
        List<AssAddrProv> provs = getProv();
        for (AssAddrProv prov : provs) {
//            System.out.println(prov);
            addrInitByProv(prov.getName());
        }
    }

    @Value("${vboot.amap.key}")
    private String AMAP_KEY;

    public List<AssAddrProv> getProv() {
        AresCoun ares = restTemplate.getForObject("https://restapi.amap.com/v3/config/district?key=" + AMAP_KEY + "&keywords=&subdistrict=1&extensions=base", AresCoun.class);
//        System.out.println(ares.toString());
        List<AssAddrProv> provs = new ArrayList<>();
        for (DistrictCoun cout : ares.getDistricts()) {
            for (DistrictProv prov : cout.getDistricts()) {
                AssAddrProv myprov = new AssAddrProv();
                myprov.setId(prov.getAdcode());
                myprov.setName(prov.getName());
                myprov.setCecoo(prov.getCenter());
                provs.add(myprov);
            }
        }
        return provs;
    }


    public void addrInitByProv(String pname) {
        AresProv ares = restTemplate.getForObject("https://restapi.amap.com/v3/config/district?key=" + AMAP_KEY + "&keywords=" + pname + "&subdistrict=3&extensions=base", AresProv.class);
        List<AssAddrProv> provs = new ArrayList<>();
        List<AssAddrCity> citys = new ArrayList<>();
        List<AssAddrDict> dicts = new ArrayList<>();
        List<AssAddrStre> stres = new ArrayList<>();
        List<AssAddrMain> addrs = new ArrayList<>();
        for (DistrictProv prov : ares.getDistricts()) {
            //省份表
            AssAddrProv myprov = new AssAddrProv();
            myprov.setId(prov.getAdcode());
            myprov.setName(prov.getName());
            myprov.setCecoo(prov.getCenter());
            provs.add(myprov);
            //集合表（包含省份、城市、地区、街道）
            AssAddrMain addrProv=new AssAddrMain();
            addrProv.setId(prov.getAdcode());
            addrProv.setName(prov.getName());
            addrProv.setCecoo(prov.getCenter());
            addrProv.setType("prov");
            addrs.add(addrProv);
            for (District city : prov.getDistricts()) {
                //城市表
                AssAddrCity mycity = new AssAddrCity();
                mycity.setId(city.getAdcode());
                mycity.setName(city.getName());
                mycity.setCecoo(city.getCenter());
                mycity.setCode(city.getCitycode());
                mycity.setProv(prov.getAdcode());
                citys.add(mycity);
                //集合表（包含省份、城市、地区、街道）
                AssAddrMain addrCity = new AssAddrMain();
                addrCity.setId(city.getAdcode());
                addrCity.setName(city.getName());
                addrCity.setCecoo(city.getCenter());
                addrCity.setCode(city.getCitycode());
                addrCity.setPid(prov.getAdcode());
                addrCity.setType("city");
                addrs.add(addrCity);
                for (District dict : city.getDistricts()) {
                    //地区表
                    AssAddrDict myDict = new AssAddrDict();
                    myDict.setId(dict.getAdcode());
                    myDict.setName(dict.getName());
                    myDict.setCecoo(dict.getCenter());
                    myDict.setCity(city.getAdcode());
                    dicts.add(myDict);
                    //集合表（包含省份、城市、地区、街道）
                    AssAddrMain addrDict = new AssAddrMain();
                    addrDict.setId(dict.getAdcode());
                    addrDict.setName(dict.getName());
                    addrDict.setCecoo(dict.getCenter());
                    addrDict.setPid(city.getAdcode());
                    addrDict.setType("dict");
                    addrs.add(addrDict);
                    for (District stre : dict.getDistricts()) {
                        //街道表
                        AssAddrStre myStre = new AssAddrStre();
                        myStre.setId(stre.getCenter());
                        myStre.setName(stre.getName());
                        myStre.setCecoo(stre.getCenter());
                        myStre.setDict(dict.getAdcode());
                        stres.add(myStre);
                        //集合表（包含省份、城市、地区、街道）
                        AssAddrMain addrStre = new AssAddrMain();
                        addrStre.setId(stre.getCenter());
                        addrStre.setName(stre.getName());
                        addrStre.setCecoo(stre.getCenter());
                        addrStre.setPid(dict.getAdcode());
                        addrStre.setType("stre");
                        addrs.add(addrStre);
                    }
                }
            }
        }

        provRepo.saveAll(provs);
        cityRepo.saveAll(citys);
        dictRepo.saveAll(dicts);
        streRepo.saveAll(stres);
        mainRepo.saveAll(addrs);

    }

    @Autowired
    private AssAddrMainRepo mainRepo;

    @Autowired
    private AssAddrProvRepo provRepo;

    @Autowired
    private AssAddrCityRepo cityRepo;

    @Autowired
    private AssAddrDictRepo dictRepo;

    @Autowired
    private AssAddrStreRepo streRepo;

    @Autowired
    private RestTemplate restTemplate;

}
