package vboot.core.module.ass.addr.pojo;

import lombok.Data;

import java.util.List;

@Data
public class Aroute {

    private String origin;

    private String destination;

    private List<Apath> paths;



}
