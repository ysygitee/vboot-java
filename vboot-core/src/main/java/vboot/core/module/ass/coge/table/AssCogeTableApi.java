package vboot.core.module.ass.coge.table;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.io.IOUtils;
import org.dom4j.DocumentException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import vboot.core.common.mvc.api.R;
import vboot.core.common.mvc.dao.Sqler;
import vboot.core.module.ass.coge.velocity.GenCode;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("ass/coge/table")
@Api(tags = {"代码生成"})
public class AssCogeTableApi {

    private String table = "ass_coge_table";

    @GetMapping
    @ApiOperation("查询代码生成主表分页")
    public R get(String name) {
        Sqler sqler = new Sqler(table);
        sqler.addLike("t.name" , name);
        return R.ok(service.findPageData(sqler));
    }

    @GetMapping("/zip")
    @ApiOperation("下载代码生成结果ZIP")
    public void getZip(HttpServletResponse response, String id) throws IOException {
        byte[] data = service.downloadCode(id);
        genCode(response, data);
    }

    @GetMapping("/show")
    @ApiOperation("预览代码生成结果")
    public R getShow(String id) throws IOException {
        List<GenCode> genCodes = service.previewCode(id);
        return R.ok(genCodes);
    }


    @GetMapping("one/{id}")
    @ApiOperation("查询代码生成主表详情")
    public R getOne(@PathVariable String id) {
        AssCogeTable main = service.findOne(id);
        return R.ok(main);
    }

    @GetMapping("list")
    @ApiOperation("查询代码生成主表列表")
    public R getList() {
        return R.ok(service.findAll());
    }

    @PostMapping
    @ApiOperation("新增代码生成主表")
    public R post(@RequestBody AssCogeTable main) throws DocumentException {
        return R.ok(service.insert(main));
    }

    @PutMapping
    @ApiOperation("修改代码生成主表")
    public R put(@RequestBody AssCogeTable main) {
        return R.ok(service.update(main));
    }

    @DeleteMapping("{ids}")
    @ApiOperation("删除代码生成主表")
    public R delete(@PathVariable String[] ids) {
        return R.ok(service.delete(ids));
    }

    @Autowired
    private AssCogeTableService service;

    //生成zip文件
    private void genCode(HttpServletResponse response, byte[] data) throws IOException {
        response.reset();
        SimpleDateFormat sdf = new SimpleDateFormat("MMddHHmmss");
        String time= sdf.format(new Date());
        String fileName="vboot-"+time+".zip";
        response.addHeader("Access-Control-Allow-Origin" , "*");
        response.addHeader("Access-Control-Expose-Headers" , "Content-Disposition,download-filename");
        response.setHeader("Content-Disposition" , "attachment; filename="+fileName);
        response.setHeader("download-filename" , fileName);
        response.addHeader("Content-Length" , "" + data.length);
        response.setContentType("application/octet-stream; charset=UTF-8");
        IOUtils.write(data, response.getOutputStream());
    }

}
