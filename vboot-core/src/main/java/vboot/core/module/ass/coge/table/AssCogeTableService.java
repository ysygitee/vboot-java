package vboot.core.module.ass.coge.table;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vboot.core.common.mvc.service.BaseMainService;
import vboot.core.module.ass.coge.velocity.GenCode;
import vboot.core.module.ass.coge.velocity.VelocityInitializer;
import vboot.core.module.ass.coge.velocity.VelocityUtils;

import javax.annotation.PostConstruct;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;


@Service
@Slf4j
public class AssCogeTableService extends BaseMainService<AssCogeTable> {

    public List<GenCode> previewCode(String id)
    {
        AssCogeTable table = repo.findById(id).get();
        List<GenCode> genCodes=new ArrayList<>();
//        Map<String, String> dataMap = new LinkedHashMap<>();
        // 查询表信息
        // 设置主子表信息
//        setSubTable(table);
//        // 设置主键列信息
//        setPkColumn(table);
        VelocityInitializer.initVelocity();

        VelocityContext context = VelocityUtils.prepareContext(table);

        // 获取模板列表
        List<String> templates = VelocityUtils.getTemplateList(table);
        for (String template : templates)
        {
            // 渲染模板
            StringWriter sw = new StringWriter();
            Template tpl = Velocity.getTemplate(template, "UTF-8");
            tpl.merge(context, sw);
            GenCode genCode=new GenCode();
            String[] arr=template.split("/");
            genCode.setTitle(arr[arr.length-1].replace(".vm",""));
            genCode.setContent( sw.toString());
            genCodes.add(genCode);
        }
        return genCodes;
    }

    public byte[] downloadCode(String id)
    {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        ZipOutputStream zip = new ZipOutputStream(outputStream);
        generatorCode(id, zip);
        IOUtils.closeQuietly(zip);
        return outputStream.toByteArray();
    }

    /**
     * 查询表信息并生成代码
     */
    private void generatorCode(String id, ZipOutputStream zip)
    {
        // 查询表信息
        AssCogeTable table = repo.findById(id).get();
        // 设置主子表信息
//        setPkColumn(table);

        VelocityInitializer.initVelocity();

        VelocityContext context = VelocityUtils.prepareContext(table);

        // 获取模板列表
//        List<String> templates = VelocityUtils.getTemplateList(table.getTplCategory());
        List<String> templates = VelocityUtils.getTemplateList(table);
        for (String template : templates)
        {
            // 渲染模板
            StringWriter sw = new StringWriter();
            Template tpl = Velocity.getTemplate(template, "UTF-8");
            tpl.merge(context, sw);
            try
            {
                // 添加到zip
                zip.putNextEntry(new ZipEntry(VelocityUtils.getFileName(template, table)));
                IOUtils.write(sw.toString(), zip, "UTF-8");
                IOUtils.closeQuietly(sw);
                zip.flush();
                zip.closeEntry();
            }
            catch (IOException e)
            {
                log.error("渲染模板失败，表名：" + table.getName(), e);
            }
        }
    }

    //----------bean注入------------
    @Autowired
    private AssCogeTableRepo repo;

    @PostConstruct
    public void initDao() {
        super.setRepo(repo);
    }
}

