package vboot.core.module.ass.coge.velocity;

import lombok.Data;

@Data
public class GenCode {

    private String type;

    private String title;

    private String path;

    private String content;
}
