package vboot.core.module.ass.num.main;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Date;

@Entity
@Data
@ApiModel("编号信息")
public class AssNumMain {
    @Id
    @Column(length = 8)
    @ApiModelProperty("主键ID")
    private String id;

    @Column(length = 100)
    @ApiModelProperty("标签")
    private String label;

    @Column(length = 32)
    @ApiModelProperty("名称")
    private String name;

    @Column(length = 32)
    @ApiModelProperty("生成模式")
    private String numod;

    @Column(length = 32)
    @ApiModelProperty("编号前缀")
    private String nupre;

    @ApiModelProperty("是否被修改过或新添加的")
    private Boolean nflag;

    @Column(length = 8)
    @ApiModelProperty("下一个编号")
    private String nunex;

    @ApiModelProperty("编号长度")
    private Integer nulen;

    @Column(length = 8)
    @ApiModelProperty("当前日期")
    private String cudat;

    @Column(updatable = false)
    @ApiModelProperty("创建时间")
    private Date crtim = new Date();

    @ApiModelProperty("更新时间")
    private Date uptim;
}
