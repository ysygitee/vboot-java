package vboot.core.module.ass.oss.file;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@ApiModel("OSS存储文件")
@Table(indexes = {@Index(columnList = "md5")})
public class AssOssFile {

    @Id
    @Column(length = 32)
    @ApiModelProperty("主键ID")
    private String id;

    @ApiModelProperty("文件md5")
    @Column(length = 32)
    private String md5;

    @ApiModelProperty("文件大小")
    private Long size;

    @ApiModelProperty("存储地址")
    private String path;

    @ApiModelProperty("存储服务")
    @Column(length = 32)
    private String service;

    @Column(updatable = false)
    @ApiModelProperty("创建时间")
    private Date crtim = new Date();

}
