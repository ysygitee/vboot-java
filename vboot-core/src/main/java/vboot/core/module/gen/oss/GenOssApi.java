package vboot.core.module.gen.oss;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import vboot.core.module.ass.oss.main.Zfile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import vboot.core.module.ass.oss.main.AssOssMainService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("gen/oss")
@Api(tags = {"存储服务"})
public class GenOssApi {

    @PostMapping(value="upload",produces = "text/html;charset=UTF-8")
    @ApiOperation("文件上传")
    public String upload(@RequestParam(value = "file", required = false) MultipartFile file) throws Exception {
        Zfile zfile = assOssMainService.uploadFile(file);
        return "{\"id\":\"" + zfile.getId() + "\",\"path\":\"" + zfile.getPath() + "\",\"filid\":\"" + zfile.getFilid() + "\",\"name\":\"" + zfile.getName() + "\",\"size\":\"" +  zfile.getSize() + "\"}";
    }

    @GetMapping("download")
    @ApiOperation("文件下载")
    public void download(String table,String id, HttpServletRequest request, HttpServletResponse response) throws Exception {
        assOssMainService.downloadFile(request,response,table,id);
    }

    @Autowired
    private AssOssMainService assOssMainService;

}
