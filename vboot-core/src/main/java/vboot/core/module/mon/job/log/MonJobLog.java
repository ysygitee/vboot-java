package vboot.core.module.mon.job.log;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Data
@ApiModel("定时任务日志")
public class MonJobLog {

    @Id
    @Column(length = 32)
    @ApiModelProperty("主键ID")
    private String id;

    @Column(length = 32)
    @ApiModelProperty("名称")
    private String name;

    @Column(length = 32)
    @ApiModelProperty("开始时间")
    private String sttim;

    @Column(length = 32)
    @ApiModelProperty("结束时间")
    private String entim;

    @Column(length = 32)
    @ApiModelProperty("运行结果")
    private String ret;

    @Column(length = 5000)
    @ApiModelProperty("日志信息")
    private String msg;
}
