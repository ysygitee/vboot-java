package vboot.core.module.mon.job.log;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import vboot.core.common.mvc.api.R;
import vboot.core.common.mvc.dao.Sqler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("mon/job/log")
@Api(tags = {"定时任务日志"})
public class MonJobLogApi {

    private String table = "mon_job_log";

    @GetMapping
    @ApiOperation("查询定时任务日志分页")
    public R get(String name) {
        Sqler sqler = new Sqler("t.id,t.name,t.sttim,t.entim,t.ret", table);
//        XreqUtil.setPageParam(sqler);
        sqler.addLike("t.name", name);
        sqler.addOrder("t.entim desc");
        System.out.println(sqler.getSql());
        return R.ok(service.findPageData(sqler));
    }

    @GetMapping("one/{id}")
    @ApiOperation("查询定时任务日志详情")
    public R getOne(@PathVariable String id) {
        return R.ok(service.findOne(id));
    }

    @DeleteMapping("all")
    @ApiOperation("清空定时任务日志")
    public R deleteAll() {
        service.deleteAll();
        return R.ok();
    }

    @DeleteMapping("{ids}")
    @ApiOperation("删除定时任务日志")
    public R delete(@PathVariable String[] ids) {
        return R.ok(service.delete(ids));
    }

    @Autowired
    private MonJobLogService service;
}
