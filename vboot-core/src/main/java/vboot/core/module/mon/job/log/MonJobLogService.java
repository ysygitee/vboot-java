package vboot.core.module.mon.job.log;

import vboot.core.common.mvc.dao.JdbcDao;
import vboot.core.common.mvc.dao.Sqler;
import lombok.extern.slf4j.Slf4j;
import vboot.core.common.mvc.api.PageData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(rollbackFor = Exception.class)
@Slf4j
public class MonJobLogService {

    @Transactional(readOnly = true)
    public PageData findPageData(Sqler sqler) {
        return jdbcDao.findPageData(sqler);
    }

    public void deleteAll(){
      repo.deleteAll();
    };


    public int delete(String[] ids) {
        for (String id : ids) {
            repo.deleteById(id);
        }
        return ids.length;
    }

    @Transactional(readOnly = true)
    public MonJobLog findOne(String id) {
        return repo.findById(id).get();
    }

    @Autowired
    private MonJobLogRepo repo;


    @Autowired
    private JdbcDao jdbcDao;
}
