package vboot.core.module.mon.log.error;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import vboot.core.common.mvc.api.R;
import vboot.core.common.mvc.dao.JdbcDao;
import vboot.core.common.mvc.dao.Sqler;

@RestController
@RequiredArgsConstructor
@RequestMapping("mon/log/error")
@Api(tags = {"错误日志"})
public class MonLogErrorApi {

    @GetMapping
    @ApiOperation("查询错误日志分页")
    public R get() {
        Sqler sqler = new Sqler("t.*", "mon_log_error");
        sqler.addOrder("t.crtim desc");
        sqler.addLeftJoin("u.name as usena,u.usnam","sys_org_user u","u.id=t.useid");
        return R.ok(jdbcDao.findPageData(sqler));
    }

    @GetMapping("one/{id}")
    @ApiOperation("查询错误日志详情")
    public R getOne(@PathVariable String id) {
        MonLogError main = service.findOne(id);
        return R.ok(main);
    }

    @DeleteMapping("{ids}")
    @ApiOperation("删除错误日志")
    public R delete(@PathVariable String[] ids) {
        return R.ok(service.delete(ids));
    }

    @DeleteMapping(value = "all")
    @ApiOperation("清空错误日志")
    public R delAllInfoLog() {
        service.delAll();
        return R.ok();
    }

    @Autowired
    private JdbcDao jdbcDao;

    private final MonLogErrorService service;


//    @OpLog("导出数据")
//    @ApiOperation("导出数据")
//    @GetMapping(value = "/download")
//    @PreAuthorize("@el.check()")
//    public void exportLog(HttpServletResponse response, LogQueryCriteria criteria) throws IOException {
//        criteria.setLogType("INFO");
//        logService.download(logService.queryAll(criteria), response);
//    }
//
//    @Log("导出错误数据")
//    @ApiOperation("导出错误数据")
//    @GetMapping(value = "/error/download")
//    @PreAuthorize("@el.check()")
//    public void exportErrorLog(HttpServletResponse response, LogQueryCriteria criteria) throws IOException {
//        criteria.setLogType("ERROR");
//        logService.download(logService.queryAll(criteria), response);
//    }
//    @GetMapping
//    @ApiOperation("日志查询")
//    @PreAuthorize("@el.check()")
//    public ResponseEntity<Object> queryLog(LogQueryCriteria criteria, Pageable pageable){
//        criteria.setLogType("INFO");
//        return new ResponseEntity<>(logService.queryAll(criteria,pageable), HttpStatus.OK);
//    }

//    @GetMapping(value = "/user")
//    @ApiOperation("用户日志查询")
//    public ResponseEntity<Object> queryUserLog(LogQueryCriteria criteria, Pageable pageable){
//        criteria.setLogType("INFO");
//        criteria.setUsername(SecurityUtils.getCurrentUsername());
//        return new ResponseEntity<>(logService.queryAllByUser(criteria,pageable), HttpStatus.OK);
//    }

    //    @GetMapping(value = "/error")
//    @ApiOperation("错误日志查询")
//    @PreAuthorize("@el.check()")
//    public ResponseEntity<Object> queryErrorLog(LogQueryCriteria criteria, Pageable pageable){
//        criteria.setLogType("ERROR");
//        return new ResponseEntity<>(logService.queryAll(criteria,pageable), HttpStatus.OK);
//    }
//
//    @GetMapping(value = "/error/{id}")
//    @ApiOperation("日志异常详情查询")
//    @PreAuthorize("@el.check()")
//    public ResponseEntity<Object> queryErrorLogDetail(@PathVariable Long id){
//        return new ResponseEntity<>(logService.findByErrDetail(id), HttpStatus.OK);
//    }
//    @DeleteMapping(value = "/del/error")
//    @Log("删除所有ERROR日志")
//    @ApiOperation("删除所有ERROR日志")
//    @PreAuthorize("@el.check()")
//    public ResponseEntity<Object> delAllErrorLog(){
//        logService.delAllByError();
//        return new ResponseEntity<>(HttpStatus.OK);
//    }
//
//    @DeleteMapping(value = "/del/info")
//    @Log("删除所有INFO日志")
//    @ApiOperation("删除所有INFO日志")
//    @PreAuthorize("@el.check()")
//    public ResponseEntity<Object> delAllInfoLog(){
//        logService.delAllByInfo();
//        return new ResponseEntity<>(HttpStatus.OK);
//    }

}
