package vboot.core.module.mon.log.error;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import vboot.core.module.mon.log.oper.MonLogOper;

@Repository
public interface MonLogErrorRepo extends JpaRepository<MonLogError,String>, JpaSpecificationExecutor<MonLogError> {

}
