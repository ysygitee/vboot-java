package vboot.core.module.mon.online.user;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.web.bind.annotation.*;
import vboot.core.common.mvc.api.R;
import vboot.core.common.mvc.dao.JdbcDao;
import vboot.core.framework.cache.RedisHandler;

import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping("mon/online/user")
@Api(tags = {"在线用户"})
public class MonOnlineUserApi {

    @Autowired
    private RedisHandler redisHandler;

    @GetMapping("list")
    @ApiOperation("查询在线用户列表")
    public R getList() {
        List<String> keyList = redisHandler.scan("online-key*");
        Collections.reverse(keyList);
        String keys="(";
        for (String key : keyList) {
            keys+="'"+key+"',";
        }
        keys = keys.substring(0, keys.length() - 1) + ")";
        String sql="select * from mon_log_login where onkey in "+keys+" order by crtim desc";
        List<OnlineUserDto> list = jdbcDao.getTp().query(sql,
                new BeanPropertyRowMapper<>(OnlineUserDto.class));
        return R.ok(list);
    }

    @PostMapping("close")
    @ApiOperation("强退在线用户")
    public R postClose(String onkey) {
        redisHandler.del(onkey);
        return R.ok();
    }

    @Autowired
    private JdbcDao jdbcDao;

}
