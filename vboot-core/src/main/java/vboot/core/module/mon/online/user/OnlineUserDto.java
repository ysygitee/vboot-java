package vboot.core.module.mon.online.user;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OnlineUserDto {

    private String id;

    //姓名
    private String name;

    private String usnam;

    private String ip;

    private String crtim;

    private String addre;

    private String ageos;

    private String agbro;

    private String agdet;

    @Column(length = 64)
    private String onkey;//online key

}
