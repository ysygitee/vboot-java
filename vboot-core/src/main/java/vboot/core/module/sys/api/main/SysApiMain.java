package vboot.core.module.sys.api.main;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import vboot.core.module.sys.portal.menu.SysPortalMenu;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Data
@ApiModel("接口信息")
public class SysApiMain {

    @Id
    @Column(length = 64)
    @ApiModelProperty("主键ID")
    private String id;

    @Column(length = 64)
    @ApiModelProperty("接口名称")
    private String name;

    @Column(length = 64)
    @ApiModelProperty("上级ID")
    private String pid;

    @Column(length = 64)
    @ApiModelProperty("接口url")
    private String url;

    @ApiModelProperty("权限代码")
    private Long code;

    @ApiModelProperty("权限位")
    private Integer pos;

//    private Boolean cotag;//公共标记

    @Column(length = 32)
    @ApiModelProperty("权限类型")
    private String type;

    @ApiModelProperty("可用标记")
    private Boolean avtag;

    @ApiModelProperty("备注")
    private String notes;

    @Column(updatable = false)
    @ApiModelProperty("创建时间")
    private Date crtim = new Date();

    @ApiModelProperty("更新时间")
    private Date uptim ;

    @Transient
    @ApiModelProperty("子接口清单")
    private List<SysApiMain> children = new ArrayList<>();

}
