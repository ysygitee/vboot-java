package vboot.core.module.sys.org.dept;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import vboot.core.common.mvc.api.R;
import vboot.core.common.mvc.dao.JdbcDao;
import vboot.core.common.mvc.dao.Sqler;
import vboot.core.common.utils.lang.XstrUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("sys/org/dept")
@Api(tags = {"组织架构-部门管理"})
public class SysOrgDeptApi {

    private String table = "sys_org_dept";

    @GetMapping
    @ApiOperation("查询部门分页")
    public R get(String name, String pid, String notes) {
        Sqler sqler = new Sqler(table);
        if (XstrUtil.isNotBlank(name)) {
            sqler.addLike("t.name" , name);
        } else {
            if ("".equals(pid)) {
                sqler.addWhere("t.pid is null");
            } else if (XstrUtil.isNotBlank(pid)) {
                sqler.addEqual("t.pid" , pid);
            }
        }
        sqler.addLike("t.notes" , notes);
        sqler.addWhere("t.avtag=1");
        sqler.addOrder("t.ornum");
        sqler.addSelect("t.ornum,t.notes,t.pid,t.crtim,t.uptim");
        return R.ok(jdbcDao.findPageData(sqler));
    }


    //排除了自己
//    @GetMapping("listn")
//    public R listn(String name, String id) {
//        Sqler sqler = new Sqler(table);
//        sqler.addLike("t.name", name);
//        sqler.addWhere("t.avtag=1");
//        sqler.addOrder("t.ornum");
//        sqler.addSelect("t.pid");
//        return R.ok(service.findWithoutItself(sqler, id));
//    }

    @GetMapping("one/{id}")
    @ApiOperation("查询部门详情")
    public R getOne(@PathVariable String id, HttpServletRequest request) {
        SysOrgDept main = service.findById(id);
        return R.ok(main);
    }


    @PostMapping
    @ApiOperation("新增部门")
    public R post(@RequestBody SysOrgDept main) {
        service.insert(main);
        return R.ok(main.getId());
    }

    @PutMapping
    @ApiOperation("修改部门")
    public R put(@RequestBody SysOrgDept main) throws Exception {
        service.update(main);
        return R.ok(main.getId());
    }

    @DeleteMapping("{ids}")
    @ApiOperation("删除部门")
    public R delete(@PathVariable String[] ids) {
        return R.ok(service.delete(ids));
    }

    @Autowired
    private JdbcDao jdbcDao;

    @Autowired
    private SysOrgDeptService service;

}
