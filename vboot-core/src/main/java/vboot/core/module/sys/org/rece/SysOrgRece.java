package vboot.core.module.sys.org.rece;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@Table(indexes = {@Index(columnList = "userid"),@Index(columnList = "orgid")})
@ApiModel("组织架构最近访问记录")
public class SysOrgRece {

    @Id
    @Column(length = 32)
    @ApiModelProperty("主键ID")
    private String id;

    @Column(length = 32)
    @ApiModelProperty("用户ID")
    private String userid;

    @Column(length = 32)
    @ApiModelProperty("最近使用的组织架构ID")
    private String orgid;

    @Column
    @ApiModelProperty("最近使用时间")
    private Date uptim = new Date();
}
