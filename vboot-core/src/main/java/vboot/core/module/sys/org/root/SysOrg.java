package vboot.core.module.sys.org.root;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Data
@ApiModel("组织架构影子")
public class SysOrg {
    @Id
    @Column(length = 32)
    @ApiModelProperty("ID")
    private String id;

    @Column(length = 100)
    @ApiModelProperty("名称")
    private String name;

    public SysOrg(String id){
        this.id=id;
    }

    public SysOrg(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public SysOrg() {

    }
}
