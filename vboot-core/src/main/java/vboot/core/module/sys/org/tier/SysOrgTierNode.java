package vboot.core.module.sys.org.tier;

import lombok.Data;
import vboot.core.module.sys.org.root.SysOrg;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

//层级节点
@Entity
@Data
public class SysOrgTierNode {

    @Id
    @Column(length = 32)
    private String id;//ID

    @Column(length = 100)
    private String name;//名称

    @Transient
    private List<SysOrgTierNode> children = new ArrayList<>(); //行项目

    @Transient
    private String pid;//父ID

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "pid")
    private SysOrgTierNode parent;//父元素

    private Integer tieid;//1为机构,2为部门,4为岗位,8为用户,16为常用群组,32为角色线

    @Column(length = 1000)
    private String tier;//层级

    private String notes;//备注

    private Integer ornum;//排序号

    @Column(updatable = false)
    private Date crtim = new Date();//创建时间

    private Date uptim;//更新时间

    private Boolean avtag;//可用标记

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "memid")
    private SysOrg member;
}
