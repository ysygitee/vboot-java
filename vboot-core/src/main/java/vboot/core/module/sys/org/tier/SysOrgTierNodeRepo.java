package vboot.core.module.sys.org.tier;

import org.springframework.data.jpa.repository.JpaRepository;

public interface SysOrgTierNodeRepo extends JpaRepository<SysOrgTierNode,String> {


}
