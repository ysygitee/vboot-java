package vboot.core.module.sys.org.tier;


import org.springframework.data.jpa.repository.JpaRepository;

public interface SysOrgTierTreeRepo extends JpaRepository<SysOrgTierTree,String> {


}

