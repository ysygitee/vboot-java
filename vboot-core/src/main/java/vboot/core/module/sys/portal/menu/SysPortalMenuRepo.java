package vboot.core.module.sys.portal.menu;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SysPortalMenuRepo extends JpaRepository<SysPortalMenu, String> {

    List<SysPortalMenu> findByNameLikeOrderByOrnum(String name);

    List<SysPortalMenu> findByOrderByOrnum();

    List<SysPortalMenu> findByPid(String pid);

}

