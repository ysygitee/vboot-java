package vboot.core.module.sys.portal.role;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vboot.core.common.mvc.service.BaseMainService;

import javax.annotation.PostConstruct;

@Service
public class SysPortalRoleService extends BaseMainService<SysPortalRole> {

    //bean注入------------------------------
    @Autowired
    private SysPortalRoleRepo repo;

    @PostConstruct
    public void initDao() {
        super.setRepo(repo);
    }
}
