package vboot.extend.oa.doc.cate;

import vboot.core.common.mvc.entity.BaseCateEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
public class OaDocCate extends BaseCateEntity {

    @ManyToOne(fetch= FetchType.EAGER)
    @JoinColumn(name = "pid")
    private OaDocCate parent;//父类别

//    @Transient
//    private List<OaDocCate> children = new ArrayList<>(); //行项目

}
