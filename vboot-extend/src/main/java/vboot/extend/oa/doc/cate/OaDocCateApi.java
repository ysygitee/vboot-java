package vboot.extend.oa.doc.cate;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import vboot.core.common.mvc.api.R;
import vboot.core.common.mvc.dao.Sqler;
import vboot.core.common.mvc.pojo.Ztree;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("oa/doc/cate")
@Api(tags = {"OA办公-文档分类"})
public class OaDocCateApi {

    private String table = "oa_doc_cate";

    @GetMapping
    @ApiOperation("查询文档分类分页")
    public R get(String name) {
        Sqler sqler = new Sqler(table);
        sqler.addSelect("t.pid,t.crtim,t.uptim");
        sqler.addLike("t.name", name);
        sqler.addOrder("t.ornum");
        return R.ok(service.findTree(sqler));
    }

    @GetMapping("one/{id}")
    @ApiOperation("查询文档分类详情")
    public R getOne(@PathVariable String id) {
        OaDocCate cate = service.findOne(id);
        return R.ok(cate);
    }

    @GetMapping("tree")
    @ApiOperation("查询文档分类树")
    public R getTree(String name) {
        List<Ztree> list = service.findTreeList(table,name);
        return R.ok(list);
    }

    @PostMapping
    @ApiOperation("新增文档分类")
    public R post(@RequestBody OaDocCate cate) {
        return R.ok(service.insert(cate));
    }

    @PutMapping
    @ApiOperation("修改文档分类")
    public R put(@RequestBody OaDocCate cate) {
        return R.ok(service.update(cate));
    }

    @DeleteMapping("{ids}")
    @ApiOperation("删除文档分类")
    public R delete(@PathVariable String[] ids) {
        return R.ok(service.delete(ids));
    }

    @Autowired
    private OaDocCateService service;

}
