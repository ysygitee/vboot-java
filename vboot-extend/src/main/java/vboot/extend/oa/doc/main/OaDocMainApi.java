package vboot.extend.oa.doc.main;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import vboot.core.common.mvc.api.R;
import vboot.core.common.mvc.dao.Sqler;
import org.dom4j.DocumentException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("oa/doc/main")
@Api(tags = {"OA办公-文档信息"})
public class OaDocMainApi {

    @GetMapping
    @ApiOperation("查询文档信息分页")
    public R get(String name) {
        Sqler sqler = new Sqler("oa_doc_main");
        sqler.addLike("t.name", name);
        return R.ok(service.findPageData(sqler));
    }

    @GetMapping("one/{id}")
    @ApiOperation("查询文档信息详情")
    public R getOne(@PathVariable String id) {
        OaDocMain main = service.findOne(id);
        return R.ok(main);
    }

    @PostMapping
    @ApiOperation("新增文档信息")
    public R post(@RequestBody OaDocMain main) throws DocumentException {
        return R.ok(service.insert(main));
    }

    @PutMapping
    @ApiOperation("修改文档信息")
    public R put(@RequestBody OaDocMain main) {
        return R.ok(service.update(main));
    }

    @DeleteMapping("{ids}")
    @ApiOperation("删除文档信息")
    public R delete(@PathVariable String[] ids) {
        return R.ok(service.delete(ids));
    }

    @Autowired
    private OaDocMainService service;

}
