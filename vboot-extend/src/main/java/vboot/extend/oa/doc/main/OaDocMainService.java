package vboot.extend.oa.doc.main;

import vboot.core.common.mvc.service.BaseMainService;
import vboot.core.module.bpm.proc.main.BpmProcMainService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
public class OaDocMainService extends BaseMainService<OaDocMain> {

    public String insert(OaDocMain OaDocMain)  {
//        BpmProcMain wfFlowMain = new BpmProcMain(dbMain);
//        flowService.start(wfFlowMain);
        return super.insert(OaDocMain);
    }

    //----------bean注入------------
    @Autowired
    private BpmProcMainService flowService;

    @Autowired
    private OaDocMainRepo repo;

    @PostConstruct
    public void initDao() {
        super.setRepo(repo);
    }

}
