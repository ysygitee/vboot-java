package vboot.extend.oa.flow.main;

import vboot.core.common.mvc.service.BaseMainService;
import vboot.core.module.bpm.proc.main.BpmProcMainService;
import vboot.core.module.bpm.proc.main.Zbpm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vboot.core.module.bpm.proc.main.Znode;
import vboot.core.common.utils.lang.XstrUtil;
import vboot.core.common.utils.web.XuserUtil;

import javax.annotation.PostConstruct;
import java.util.Date;

@Service
public class OaFlowMainService extends BaseMainService<OaFlowMain> {

    public void insertx(OaFlowMain oaFlowMain) {
        oaFlowMain.setState("20");
        oaFlowMain.setId(XstrUtil.getUUID());
        oaFlowMain.setUptim(new Date());
        if (oaFlowMain.getCrman() == null) {
            oaFlowMain.setCrman(XuserUtil.getUser());
        }
        OaFlowMain dbMain = repo.save(oaFlowMain);

        Zbpm zbpm = oaFlowMain.getZbpm();
        zbpm.setProid(dbMain.getId());
        zbpm.setProna(dbMain.getName());
        zbpm.setHaman(dbMain.getCrman().getId());
        zbpm.setTemid(dbMain.getProtd());
        Znode znode = procService.start(zbpm);
        if ("N3".equals(znode.getFacno())) {
            dbMain.setState("30");
            super.update(dbMain);
        }
    }

    public void updatex(OaFlowMain oaFlowMain) {
        oaFlowMain.setState("20");
        super.update(oaFlowMain);
        if ("pass".equals(oaFlowMain.getZbpm().getOpkey())) {
            Znode znode = procService.handlerPass(oaFlowMain.getZbpm());
            if ("N3".equals(znode.getFacno())) {
                oaFlowMain.setState("30");
                super.update(oaFlowMain);
            }
        } else if ("refuse".equals(oaFlowMain.getZbpm().getOpkey())) {
            if ("N2".equals(oaFlowMain.getZbpm().getTarno())) {
                oaFlowMain.setState("11");
                super.update(oaFlowMain);
            }
            Znode znode = procService.handlerRefuse(oaFlowMain.getZbpm());
        }
    }

    //----------bean注入------------
    @Autowired
    private BpmProcMainService procService;

    @Autowired
    private OaFlowMainRepo repo;

    @PostConstruct
    public void initDao() {
        super.setRepo(repo);
    }

}
