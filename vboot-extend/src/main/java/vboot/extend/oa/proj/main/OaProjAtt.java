package vboot.extend.oa.proj.main;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

//主记录OneToMany
@Entity
@Data
@ApiModel("项目附件")
public class OaProjAtt
{
    @Id
    @Column(length = 32)
    @ApiModelProperty("主键ID")
    private String id;

    @ApiModelProperty("附件名称")
    private String name;

    @ApiModelProperty("附件大小")
    private String size;

    @ApiModelProperty("存储地址")
    private String path;

    @ApiModelProperty("文件ID")
    @Column(length = 32)
    private String filid;

    @ApiModelProperty("业务ID")
    @Column(length = 32)
    private String busid;

    @ApiModelProperty("排序号")
    private Integer ornum;

}
