package vboot.extend.oa.proj.main;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import vboot.core.common.mvc.entity.BaseMainEntity;
import vboot.core.module.sys.org.root.SysOrg;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
@ApiModel("项目")
public class OaProjMain extends BaseMainEntity {

    //流水号示例
    @Column(length = 32)
    @ApiModelProperty("流水号")
    private String senum;

    //ManyToOne示例
    @ManyToOne
    @JoinColumn(name = "prman")
    @ApiModelProperty("负责人")
    private SysOrg prman;

    //ManyToMany示例
    @ManyToMany
    @JoinTable(name = "oa_proj_viewer", joinColumns = {@JoinColumn(name = "mid")},
            inverseJoinColumns = {@JoinColumn(name = "oid")})
    @ApiModelProperty("可查看者")
    private List<SysOrg> viewers;

    //ManyToMany示例
    @ManyToMany
    @JoinTable(name = "oa_proj_editor", joinColumns = {@JoinColumn(name = "mid")},
            inverseJoinColumns = {@JoinColumn(name = "oid")})
    @ApiModelProperty("可编辑者")
    private List<SysOrg> editors;

    //OneToMany示例
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "maiid")
    @OrderBy("ornum ASC")
    @ApiModelProperty("项目成员")
    private List<OaProjMember> members = new ArrayList<>();

    //附件示例
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "maiid")
    @ApiModelProperty("附件清单")
    private List<OaProjAtt> atts = new ArrayList<>();
}
