package vboot.extend.oa.proj.main;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import vboot.core.common.mvc.api.R;
import vboot.core.common.mvc.dao.JdbcDao;
import vboot.core.common.mvc.dao.Sqler;
import vboot.core.module.ass.num.main.AssNumMainService;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("oa/proj/main")
@Api(tags = {"项目管理"})
public class OaProjMainApi {

    @GetMapping
    @ApiOperation("查询项目分页")
    public R get(String name) {
        Sqler sqler = new Sqler("oa_proj_main");
        sqler.addLike("t.name", name);
        sqler.addSelect("t.senum,t.notes");
        return R.ok(service.findPageData(sqler));
    }

    @GetMapping("list")
    public R getList() {
        Sqler sqler = new Sqler("oa_proj_main");
        return R.ok(jdbcDao.findIdNameList(sqler));
    }

    @GetMapping("one/{id}")
    @ApiOperation("查询项目详情")
    public R getOne(@PathVariable String id) {
        OaProjMain main = service.findOne(id);
        return R.ok(main);
    }

    @PostMapping("impdd")
    @ApiOperation("钉钉旧项目导入")
    public R impdd(@RequestParam(value = "file", required = false) MultipartFile file) {
        service.impDd(file);
        return R.ok();
    }


    @PostMapping
    @ApiOperation("新增项目")
    public R post(@RequestBody OaProjMain main) {
        main.setSenum(numService.getNum("PROJ"));//设置供应商流水号
        return R.ok(service.insert(main));
    }

    @PutMapping
    @ApiOperation("更新项目")
    public R put(@RequestBody OaProjMain main) {
        return R.ok(service.update(main));
    }

    @DeleteMapping("{ids}")
    @ApiOperation("删除项目")
    public R delete(@PathVariable String[] ids) {
        return R.ok(service.delete(ids));
    }

    @Autowired
    private OaProjMainService service;

    @Autowired
    private JdbcDao jdbcDao;

    @Autowired
    private AssNumMainService numService;

}
