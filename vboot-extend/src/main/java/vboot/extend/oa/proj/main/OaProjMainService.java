package vboot.extend.oa.proj.main;

import cn.hutool.crypto.digest.DigestUtil;
import lombok.SneakyThrows;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import vboot.core.common.mvc.pojo.ZidName;
import vboot.core.common.mvc.service.BaseMainService;
import vboot.core.common.utils.file.XexcelUtil;
import vboot.core.common.utils.lang.XdateUtil;
import vboot.core.common.utils.lang.XstrUtil;
import vboot.core.common.utils.lang.XtimeUtil;
import vboot.core.module.sys.org.root.SysOrg;
import vboot.extend.oa.proj.task.OaProjTask;
import vboot.extend.oa.proj.task.OaProjTaskRepo;
import vboot.extend.oa.proj.tlist.OaProjTlist;
import vboot.extend.oa.proj.tlist.OaProjTlistRepo;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class OaProjMainService extends BaseMainService<OaProjMain> {

    public void impDd(MultipartFile file) {
        Workbook workbook = null;
        List<Map<String, Object>> list = new ArrayList<>();
        try {
            workbook = XexcelUtil.getWorkbook(file);
            Sheet sheet = workbook.getSheetAt(0);
            //第1行是字段名称
            Row row0 = sheet.getRow(1);
            //列数
            int cellNum = row0.getLastCellNum();
            //行数
            int rowNum = sheet.getLastRowNum();

            //从第3行开始导入
            for (int i = 2; i <= rowNum; i++) {
                Row row = sheet.getRow(i);
                if (row.getCell(0) == null || "".equals(row.getCell(0))) {
                    break;
                }
                Map<String, Object> map = new HashMap<String, Object>();
                for (int j = 0; j < cellNum; j++) {
                    map.put(XexcelUtil.getCellValue(row0.getCell(j)) + "", XexcelUtil.getCellValue(row.getCell(j)));
                }
                list.add(map);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            XexcelUtil.closeWorkBook(workbook);
        }
        impToOaProj(list);
    }

    @SneakyThrows
    private void impToOaProj(List<Map<String, Object>> list)  {
        OaProjMain main=new OaProjMain();
        String projid=""+list.get(0).get("项目ID");
        main.setId(projid.substring(4));
        main.setName(""+list.get(0).get("项目名称"));
        repo.save(main);
        String lastStage = "";
        List<OaProjTlist> tlists=new ArrayList<>();
        List<OaProjTask> tasks=new ArrayList<>();
        int i=1;
        int j=1;
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Set<String> prnams=new HashSet<>();
        for (Map<String, Object> map : list) {
            if(!lastStage.equals(""+map.get("阶段"))){
                OaProjTlist tlist=new OaProjTlist();
                tlist.setId(DigestUtil.md5Hex(main.getId()+map.get("阶段")));
                tlist.setName(""+map.get("阶段"));
                tlist.setProid(main.getId());
                tlist.setAvtag(true);
                tlist.setOrnum(i++);
                tlists.add(tlist);
                lastStage=""+map.get("阶段");
                j=1;
            }
            OaProjTask task=new OaProjTask();
            if(XstrUtil.isNotBlank(""+map.get("三级事件"))){
                String fname=main.getId()+map.get("阶段")+map.get("一级事件")+map.get("二级事件")+map.get("三级事件");
                String pname=main.getId()+map.get("阶段")+map.get("一级事件")+map.get("二级事件");
                task.setId(DigestUtil.md5Hex(fname));
                task.setName(""+map.get("三级事件"));
                task.setCont(""+map.get("三级事件描述"));
                task.setParent(new OaProjTask(DigestUtil.md5Hex(pname)));
                task.setTliid(DigestUtil.md5Hex(main.getId()+map.get("阶段")));
            }else if(XstrUtil.isNotBlank(""+map.get("二级事件"))){
                String fname=main.getId()+map.get("阶段")+map.get("一级事件")+map.get("二级事件");
                String pname=main.getId()+map.get("阶段")+map.get("一级事件");
                task.setId(DigestUtil.md5Hex(fname));
                task.setName(""+map.get("二级事件"));
                task.setCont(""+map.get("二级事件描述"));
                task.setParent(new OaProjTask(DigestUtil.md5Hex(pname)));
                task.setTliid(DigestUtil.md5Hex(main.getId()+map.get("阶段")));
            }else if(XstrUtil.isNotBlank(""+map.get("一级事件"))){
                String fname=main.getId()+map.get("阶段")+map.get("一级事件");
                task.setId(DigestUtil.md5Hex(fname));
                task.setName(""+map.get("一级事件"));
                task.setCont(""+map.get("一级事件描述"));
                task.setTliid(DigestUtil.md5Hex(main.getId()+map.get("阶段")));
            }
            String sttim = "" + map.get("开始时间");
            if(XstrUtil.isNotBlank(sttim)){
                task.setSttim(formatter.parse(sttim));
                task.setCrtim(task.getCrtim());
            }
            String cltim = "" + map.get("结束时间");
            if(XstrUtil.isNotBlank(cltim)){
                task.setCltim(formatter.parse(cltim));
            }
            String fitim = "" + map.get("完成时间");
            if(XstrUtil.isNotBlank(fitim)){
                task.setFitim(formatter.parse(fitim));
            }
            task.setState("" + map.get("事件状态"));
            String prnam="" + map.get("负责人(角色)");
            if(XstrUtil.isNotBlank(prnam)){
                prnam=prnam.split("（")[0];
                task.setPrnam(prnam);
                prnams.add(prnam);
            }
            task.setAvtag(true);
            task.setGrade(map.get("优先级")!=null?""+map.get("优先级"):null);
            task.setOrnum(j++);
            tasks.add(task);
        }
        String prnamConds="";
        for (String prnam : prnams) {
            prnamConds+="'"+prnam+"',";
        }
        if(!"".equals(prnamConds)){
            prnamConds=prnamConds.substring(0,prnamConds.length()-1);
        }
        String sql="select id,name from sys_org_user where name in ("+prnamConds+")";
        List<ZidName> idNameList = jdbcDao.findIdNameList(sql);
        for (OaProjTask task : tasks) {
            if(task.getPrnam()!=null){
                boolean flag=false;
                for (ZidName zidName : idNameList) {
                    if(zidName.getName().equals(task.getPrnam())){
                        task.setPrman(new SysOrg(zidName.getId()));
                        flag=true;
                        break;
                    }
                }
                if(!flag){
                    System.out.println(task.getPrnam());
                }
            }
        }
        tlistRepo.saveAll(tlists);
        taskRepo.saveAll(tasks);
    }

    @Autowired
    private OaProjMainRepo repo;

    @Autowired
    private OaProjTlistRepo tlistRepo;

    @Autowired
    private OaProjTaskRepo taskRepo;

    @PostConstruct
    public void initDao() {
        super.setRepo(repo);
    }

}

