package vboot.extend.oa.proj.task;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import vboot.core.common.mvc.entity.BaseCateEntity;
import vboot.core.module.sys.org.root.SysOrg;

import javax.persistence.*;
import java.util.Date;

@Entity
@Getter
@Setter
public class OaProjTask extends BaseCateEntity {

    @ManyToOne(fetch= FetchType.EAGER)
    @JoinColumn(name = "pid")
    @ApiModelProperty("父类别")
    private OaProjTask parent;

    @Column(length = 36)
    @ApiModelProperty("任务清单ID")
    private String tliid;

    @Column(length = 32)
    @ApiModelProperty("状态")
    private String state;

    @ManyToOne
    @JoinColumn(name = "prman")
    @ApiModelProperty("责任人")
    private SysOrg prman;

    @Column(length = 32)
    @ApiModelProperty("优先级")
    private String grade;

    @Transient
    private String prnam;

    @Column(length = 2000)
    @ApiModelProperty("任务内容")
    private String cont;

    @ApiModelProperty("截止时间")
    private Date cltim;

    @ApiModelProperty("开始时间")
    private Date sttim;

    @ApiModelProperty("完成时间")
    private Date fitim;

    public OaProjTask() {

    }

    public OaProjTask(String id) {
        this.id=id;
    }
}