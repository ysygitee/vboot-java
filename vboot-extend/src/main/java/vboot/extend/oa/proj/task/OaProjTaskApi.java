package vboot.extend.oa.proj.task;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.web.bind.annotation.*;
import vboot.core.common.mvc.api.PageData;
import vboot.core.common.mvc.api.R;
import vboot.core.common.mvc.dao.JdbcDao;
import vboot.core.common.mvc.dao.Sqler;
import vboot.core.common.mvc.pojo.Ztree;
import vboot.core.common.utils.lang.XstrUtil;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("oa/proj/task")
public class OaProjTaskApi {


    private String table = "oa_proj_task";

    @GetMapping("treez")
    @ApiOperation("查询任务除自身外的任务树")
    public R getTreez(String name, String id) {
        List<Ztree> list = service.tier2Choose(table, id, name);
        return R.ok(list);
    }

    @GetMapping("treea")
    @ApiOperation("查询任务树")
    public R getTreea(String name) {
        List<Ztree> list = service.findTreeList(table, name);
        return R.ok(list);
    }

    @GetMapping("tree")
    @ApiOperation("查询任务列表")
    public R getTree(String name) {
        Sqler sqler = new Sqler(table);
        sqler.addLike("t.name", name);
        sqler.addOrder("t.ornum");
        sqler.addDescOrder("t.crtim");
        sqler.addWhere("t.avtag=1");
        sqler.addSelect("t.crtim,t.uptim,t.pid,t.notes");
        List<OaProjTask> list = service.findTree(sqler);
        return R.ok(list);
    }

    @GetMapping("list")
    @ApiOperation("查询任务")
    public R getList(String name,String proid,String tliid) {
        if(XstrUtil.isBlank(tliid)){
            return R.ok(new ArrayList<>());
        }else{
            Sqler sqler=new Sqler("oa_proj_task");
            sqler.addLeftJoin("o.name prman","sys_org o","o.id=t.prman");
            sqler.addSelect("t.pid,DATE_FORMAT(t.sttim,'%Y-%m-%d') sttim,DATE_FORMAT(t.crtim,'%Y-%m-%d') crtim," +
                    "DATE_FORMAT(t.cltim,'%Y-%m-%d') cltim,DATE_FORMAT(t.fitim,'%Y-%m-%d') fitim");
            sqler.addEqual("t.tliid", tliid);
            sqler.addOrder("t.ornum");
            sqler.addDescOrder("t.crtim");
            sqler.addSelect("t.grade,t.state");
            List<OaProjTaskVo> taskVoList = jdbcDao.getTp().query(sqler.getSql(), sqler.getParams(),
                    new BeanPropertyRowMapper<>(OaProjTaskVo.class));
            return R.ok(taskVoList);
        }
    }

    @GetMapping
    @ApiOperation("查询任务分页")
    public R get(String name,String proid,String tliid) {
        if(XstrUtil.isBlank(tliid)){
            return R.ok(new PageData(0, new ArrayList<>()));
        }else{
            Sqler sqler = new Sqler(table);
            sqler.addLike("t.name", name);
            sqler.addSelect("t.notes,t.ornum");
            sqler.addEqual("t.tliid",tliid);
            return R.ok(service.findPageData(sqler));
        }
    }

    @GetMapping("one/{id}")
    @ApiOperation("查询任务详情")
    public R getOne(@PathVariable String id) {
        OaProjTask cate = service.findOne(id);
        return R.ok(cate);
    }

    @PostMapping
    @ApiOperation("新增任务")
    public R post(@RequestBody OaProjTask cate) {
        return R.ok(service.insert(cate));
    }

    @PutMapping
    @ApiOperation("更新任务")
    public R put(@RequestBody OaProjTask cate) {
        return R.ok(service.update(cate));
    }

    @DeleteMapping("{ids}")
    @ApiOperation("删除任务")
    public R delete(@PathVariable String[] ids) {
        return R.ok(service.delete(ids));
    }


    @Autowired
    private OaProjTaskService service;

    @Autowired
    private JdbcDao jdbcDao;

}