package vboot.extend.oa.proj.task;

import org.springframework.data.jpa.repository.JpaRepository;

public interface OaProjTaskRepo extends JpaRepository<OaProjTask,String> {

}
