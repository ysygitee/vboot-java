package vboot.extend.oa.proj.task;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Service;
import vboot.core.common.mvc.dao.Sqler;
import vboot.core.common.mvc.service.BaseCateService;

import javax.annotation.PostConstruct;
import java.util.List;

@Service
public class OaProjTaskService extends BaseCateService<OaProjTask> {

    public List<OaProjTask> findTree(Sqler sqler) {
        List<OaProjTask> list = jdbcDao.getTp().query(sqler.getSql(), sqler.getParams(),
                new BeanPropertyRowMapper<>(OaProjTask.class));
        return buildByRecursive(list);
    }

    @Autowired
    private OaProjTaskRepo repo;

    @PostConstruct
    public void initDao() {
        super.setRepo(repo);
    }
}
