package vboot.extend.oa.proj.task;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OaProjTaskVo{

    private String prman;

    private String crtim;

    private String cltim;

    private String sttim;

    private String fitim;

    private String state;

    private String grade;

    private String id;

    private String name;

    private String pid;
}
