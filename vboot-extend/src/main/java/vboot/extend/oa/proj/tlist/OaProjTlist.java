package vboot.extend.oa.proj.tlist;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import vboot.core.common.mvc.entity.BaseCateEntity;

import javax.persistence.*;

@Entity
@Getter
@Setter
public class OaProjTlist extends BaseCateEntity {

    @ManyToOne(fetch= FetchType.EAGER)
    @JoinColumn(name = "pid")
    @ApiModelProperty("父类别")
    private OaProjTlist parent;

    @Column(length = 36)
    @ApiModelProperty("项目ID")
    private String proid;

}