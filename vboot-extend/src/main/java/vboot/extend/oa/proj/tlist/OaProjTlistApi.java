package vboot.extend.oa.proj.tlist;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import vboot.core.common.mvc.api.R;
import vboot.core.common.mvc.dao.Sqler;
import vboot.core.common.mvc.pojo.Ztree;

import java.util.List;

@RestController
@RequestMapping("oa/proj/tlist")
public class OaProjTlistApi {


    private String table = "oa_proj_tlist";

    @GetMapping("treez")
    @ApiOperation("查询任务清单除自身外的树")
    public R getTreez(String name, String id) {
        List<Ztree> list = service.tier2Choose(table, id, name);
        return R.ok(list);
    }

    @GetMapping("treea")
    @ApiOperation("查询任务清单树")
    public R getTreea(String name) {
        List<Ztree> list = service.findTreeList(table, name);
        return R.ok(list);
    }

    @GetMapping("tree")
    @ApiOperation("查询任务清单列表")
    public R getTree(String proid) {
        Sqler sqler = new Sqler(table);
        sqler.addEqual("t.proid", proid);
        sqler.addOrder("t.ornum");
        sqler.addWhere("t.avtag=1");
        sqler.addSelect("t.crtim,t.uptim,t.pid,t.notes");
        List<OaProjTlist> list = service.findTree(sqler);
        return R.ok(list);
    }

    @GetMapping
    @ApiOperation("查询任务清单分页")
    public R get(String name) {
        Sqler sqler = new Sqler(table);
        sqler.addLike("t.name", name);
        sqler.addSelect("t.notes,t.ornum");
        return R.ok(service.findPageData(sqler));
    }

    @GetMapping("one/{id}")
    @ApiOperation("查询任务清单详情")
    public R getOne(@PathVariable String id) {
        OaProjTlist cate = service.findOne(id);
        return R.ok(cate);
    }

    @PostMapping
    @ApiOperation("新增任务清单")
    public R post(@RequestBody OaProjTlist cate) {
        return R.ok(service.insert(cate));
    }

    @PutMapping
    @ApiOperation("更新任务清单")
    public R put(@RequestBody OaProjTlist cate) {
        return R.ok(service.update(cate));
    }

    @DeleteMapping("{ids}")
    @ApiOperation("删除任务清单")
    public R delete(@PathVariable String[] ids) {
        return R.ok(service.delete(ids));
    }

    @Autowired
    private OaProjTlistService service;

}