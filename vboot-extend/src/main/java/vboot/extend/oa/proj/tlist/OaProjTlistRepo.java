package vboot.extend.oa.proj.tlist;

import org.springframework.data.jpa.repository.JpaRepository;

public interface OaProjTlistRepo extends JpaRepository<OaProjTlist,String> {

}
