package vboot.extend.oa.proj.tlist;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Service;
import vboot.core.common.mvc.dao.Sqler;
import vboot.core.common.mvc.service.BaseCateService;

import javax.annotation.PostConstruct;
import java.util.List;

@Service
public class OaProjTlistService extends BaseCateService<OaProjTlist> {

    public List<OaProjTlist> findTree(Sqler sqler) {
        List<OaProjTlist> list = jdbcDao.getTp().query(sqler.getSql(), sqler.getParams(),
                new BeanPropertyRowMapper<>(OaProjTlist.class));
        return buildByRecursive(list);
    }

    @Autowired
    private OaProjTlistRepo repo;

    @PostConstruct
    public void initDao() {
        super.setRepo(repo);
    }
}
