package vboot.extend.oa.test.one;

import cn.hutool.script.ScriptRuntimeException;
import cn.hutool.script.ScriptUtil;
import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.write.builder.ExcelWriterBuilder;
import com.alibaba.excel.write.builder.ExcelWriterSheetBuilder;
import com.alibaba.excel.write.style.column.LongestMatchColumnWidthStyleStrategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import vboot.core.common.mvc.api.R;
import vboot.core.common.mvc.dao.Sqler;
import vboot.core.module.mon.log.oper.Oplog;
import vboot.core.common.utils.file.ExcelUtil;
import vboot.core.common.utils.file.FileUtils;
import vboot.core.common.utils.file.excel.ExcelResult;
import vboot.core.common.utils.lang.XnumberUtil;

import javax.script.CompiledScript;
import javax.script.ScriptException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("oa/test/one")
public class OaTestOneApi {

    @Oplog("查询DEMO")
    @GetMapping
    public R get(String name, String catid) throws Exception {
        CompiledScript script = ScriptUtil.compile("var map={};var a=1;var b=2;map.xx="
                + XnumberUtil.getRandom6() +"+a+b;map");
        try {
            Map<String,Object> map= (Map<String, Object>) script.eval();
            System.out.println(map.get("xx"));
        } catch (ScriptException e) {
            throw new ScriptRuntimeException(e);
        }
//        int i=1/0;
//        if(true){
//            throw new Exception("错误了");
//        }
        Sqler sqler = new Sqler("oa_test_one");
        sqler.addLike("t.name", name);
        sqler.addSelect("t.addre,t.senum");
        return R.ok(service.findPageData(sqler));
    }

    @PostMapping("imp")
    public R postImp(@RequestPart("file") MultipartFile file) throws Exception {
        ExcelResult<Student> result = ExcelUtil.importExcel(file.getInputStream(), Student.class, new StudentImportListener());
        System.out.println(result);
        return R.ok(result);
//        return R.error(result);
    }


    @GetMapping("exp")
    public void getExp(HttpServletResponse response) throws Exception {
        response.setContentType("application/vnd.ms-excel");
        response.setCharacterEncoding("utf-8");
        String fileName = "测试1ss.xlsx";
        FileUtils.setAttachmentResponseHeader(response, fileName);
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8");
        ServletOutputStream outputStream = response.getOutputStream();
        ExcelWriterBuilder writeWorkBook = EasyExcel.write(outputStream, Student.class)
                .registerWriteHandler((new LongestMatchColumnWidthStyleStrategy()));
        ExcelWriterSheetBuilder sheet = writeWorkBook.sheet();
        sheet.doWrite(initData());
    }

    private static List<Student> initData(){
        List<Student> students=new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            Student data=new Student();
            data.setName("测试"+i+2);
            data.setGender("男");
            data.setBirthday(new Date());
            students.add(data);
        }
        System.out.println(students);
        return students;
    }

    @GetMapping("one/{id}")
    public R getOne(@PathVariable String id) {
        OaTestOne main = service.findOne(id);
        return R.ok(main);
    }

    @PostMapping
    public R post(@RequestBody OaTestOne main) {
        return R.ok(service.insert(main));
    }

    @PutMapping
    public R put(@RequestBody OaTestOne main) {
        return R.ok(service.update(main));
    }

    @DeleteMapping("{ids}")
    public R delete(@PathVariable String[] ids) {
        return R.ok(service.delete(ids));
    }


    @Autowired
    private OaTestOneService service;

}
