package vboot.extend.oa.test.two;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import vboot.core.common.mvc.api.R;
import vboot.core.common.mvc.dao.JdbcDao;
import vboot.core.common.mvc.dao.Sqler;

@RestController
@RequestMapping("oa/test/two")
public class OaTestTwoApi {

    @GetMapping("list")
    public R getList() {
        return R.ok(service.findAll());
    }

    //分页查询项目清单
    @GetMapping
    public R get(String name) {
        Sqler sqler = new Sqler("oa_test_two");
        sqler.addLike("t.name", name);
        sqler.addSelect("t.notes");
        return R.ok(jdbcDao.findPageData(sqler));
    }

    //查询单个项目
    @GetMapping("one/{id}")
    public R getOne(@PathVariable String id) {
        OaTestTwo main = service.findOne(id);
        return R.ok(main);
    }

    //新增项目
    @PostMapping
    public R post(@RequestBody OaTestTwo main) {
        return R.ok(service.insert(main));
    }

    //修改项目
    @PutMapping
    public R put(@RequestBody OaTestTwo main) {
        return R.ok(service.update(main));
    }

    //删除项目
    @DeleteMapping("{ids}")
    public R delete(@PathVariable String[] ids) {
        return R.ok(service.delete(ids));
    }


    //同时使用JPA与mybatis-plus的事务测试
    @PostMapping("test")
    public R postTest() {
        service.test();
        return R.ok();
    }

    @Autowired
    private OaTestTwoService service;

    @Autowired
    private JdbcDao jdbcDao;

}
