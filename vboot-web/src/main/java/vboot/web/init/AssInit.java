package vboot.web.init;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import vboot.core.module.ass.dict.data.AssDictData;
import vboot.core.module.ass.dict.data.AssDictDataRepo;
import vboot.core.module.ass.dict.main.AssDictMain;
import vboot.core.module.ass.dict.main.AssDictMainRepo;
import vboot.core.module.ass.num.main.AssNumMain;
import vboot.core.module.ass.num.main.AssNumMainRepo;
import vboot.core.common.utils.lang.XstrUtil;

import java.util.ArrayList;
import java.util.List;

//辅助数据初始化
@Component
public class AssInit {

    protected void initData() throws Exception {
        initNum();
        initDict();
    }

    //流水号初始化
    private void initNum() {
        List<AssNumMain> numList = new ArrayList<>();
        AssNumMain suppNum = new AssNumMain();
        suppNum.setId("DEMO");
        suppNum.setName("DEMO流水号");
        suppNum.setNumod("yyyymmdd");
        suppNum.setNulen(3);
        suppNum.setNflag(true);
        suppNum.setNupre("DE");
        numList.add(suppNum);

        AssNumMain oaProjNum = new AssNumMain();
        oaProjNum.setId("OAPROJ");
        oaProjNum.setName("OA项目流水号");
        oaProjNum.setNumod("yyyymmdd");
        oaProjNum.setNulen(3);
        oaProjNum.setNflag(true);
        oaProjNum.setNupre("OAPROJ");
        numList.add(oaProjNum);

        AssNumMain projNum = new AssNumMain();
        projNum.setId("PROJ");
        projNum.setName("CRM项目流水号");
        projNum.setNumod("yyyymmdd");
        projNum.setNulen(3);
        projNum.setNflag(true);
        projNum.setNupre("PR");
        numList.add(projNum);

        AssNumMain custNum = new AssNumMain();
        custNum.setId("CUST");
        custNum.setName("CRM客户流水号");
        custNum.setNumod("yyyymmdd");
        custNum.setNulen(3);
        custNum.setNflag(true);
        custNum.setNupre("CU");
        numList.add(custNum);

        AssNumMain agentNum = new AssNumMain();
        agentNum.setId("AGENT");
        agentNum.setName("CRM代理商流水号");
        agentNum.setNumod("yyyymmdd");
        agentNum.setNulen(3);
        agentNum.setNflag(true);
        agentNum.setNupre("AG");
        numList.add(agentNum);
        numRepo.saveAll(numList);

    }

    //数据字典初始化
    private void initDict() {
        List<AssDictMain> dictList = new ArrayList<>();

        AssDictMain demoDict = new AssDictMain();
        demoDict.setId("DE_GRADE");
        demoDict.setName("DEMO等级");
        demoDict.setAvtag(true);
        dictList.add(demoDict);

        AssDictMain agentDict = new AssDictMain();
        agentDict.setId("AG_LEVEL");
        agentDict.setName("代理商资质等级");
        agentDict.setAvtag(true);
        dictList.add(agentDict);

        dictRepo.saveAll(dictList);

        List<AssDictData> dictDataList = new ArrayList<>();
        AssDictData demo1 = new AssDictData();
        demo1.setId(XstrUtil.getUUID());
        demo1.setCode("A");
        demo1.setName("等级A");
        demo1.setAvtag(true);
        demo1.setOrnum(1);
        demo1.setMaiid("DE_GRADE");
        dictDataList.add(demo1);

        AssDictData demo2 = new AssDictData();
        demo2.setId(XstrUtil.getUUID());
        demo2.setCode("B");
        demo2.setName("等级B");
        demo2.setAvtag(true);
        demo2.setOrnum(2);
        demo2.setMaiid("DE_GRADE");
        dictDataList.add(demo2);

        AssDictData demo3 = new AssDictData();
        demo3.setId(XstrUtil.getUUID());
        demo3.setCode("C");
        demo3.setName("等级C");
        demo3.setAvtag(true);
        demo3.setOrnum(3);
        demo3.setMaiid("DE_GRADE");
        dictDataList.add(demo3);

        AssDictData demo4 = new AssDictData();
        demo4.setId(XstrUtil.getUUID());
        demo4.setCode("Z");
        demo4.setName("不合格");
        demo4.setAvtag(true);
        demo4.setOrnum(4);
        demo4.setMaiid("DE_GRADE");
        dictDataList.add(demo4);

        AssDictData agent1 = new AssDictData();
        agent1.setId(XstrUtil.getUUID());
        agent1.setCode("A");
        agent1.setName("A级资质");
        agent1.setAvtag(true);
        agent1.setOrnum(1);
        agent1.setMaiid("AG_LEVEL");
        dictDataList.add(agent1);

        AssDictData agent2 = new AssDictData();
        agent2.setId(XstrUtil.getUUID());
        agent2.setCode("B");
        agent2.setName("B级资质");
        agent2.setAvtag(true);
        agent2.setOrnum(2);
        agent2.setMaiid("AG_LEVEL");
        dictDataList.add(agent2);

        AssDictData agent3 = new AssDictData();
        agent3.setId(XstrUtil.getUUID());
        agent3.setCode("C");
        agent3.setName("C级资质");
        agent3.setAvtag(true);
        agent3.setOrnum(3);
        agent3.setMaiid("AG_LEVEL");
        dictDataList.add(agent3);

        AssDictData agent4 = new AssDictData();
        agent4.setId(XstrUtil.getUUID());
        agent4.setCode("Z");
        agent4.setName("不合格");
        agent4.setAvtag(true);
        agent4.setOrnum(4);
        agent4.setMaiid("AG_LEVEL");
        dictDataList.add(agent4);

        dictDataRepo.saveAll(dictDataList);
    }

    @Autowired
    private AssDictMainRepo dictRepo;

    @Autowired
    private AssDictDataRepo dictDataRepo;

    @Autowired
    private AssNumMainRepo numRepo;
}
