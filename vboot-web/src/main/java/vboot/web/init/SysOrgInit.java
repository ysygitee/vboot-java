package vboot.web.init;

import vboot.core.common.utils.lang.XstrUtil;
import vboot.core.module.sys.org.dept.SysOrgDept;
import vboot.core.module.sys.org.dept.SysOrgDeptService;
import vboot.core.module.sys.org.group.SysOrgGroup;
import vboot.core.module.sys.org.group.SysOrgGroupService;
import vboot.core.module.sys.org.root.SysOrg;
import vboot.core.module.sys.org.user.SysOrgUser;
import vboot.core.module.sys.org.user.SysOrgUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

//组织架构初始化，可根据application.properties的app.init.type配置生成不同的基础数据
@Component
public class SysOrgInit {

    //初始化部门
    protected void initDept() {
        List<SysOrgDept> deptList = new ArrayList<>();
        SysOrgDept wc=new SysOrgDept();
        wc.setId("wc");
        wc.setName("WC集团");
        wc.setLabel("wc");
        wc.setOrnum(0);
        wc.setAvtag(true);
        wc.setTier("xwcx");
        wc.setType(1);
        deptList.add(wc);

        SysOrgDept wca=new SysOrgDept();
        wca.setId("a");
        wca.setName("A组_卡厄塞荷");
        wca.setLabel("wc");
        wca.setOrnum(1);
        wca.setAvtag(true);
        wca.setTier("xwcxax");
        wca.setType(1);
        wca.setParent(new SysOrg("wc"));
        deptList.add(wca);

        SysOrgDept a1=new SysOrgDept();
        a1.setId("a1");
        a1.setName("卡塔尔");
        a1.setLabel("wc");
        a1.setOrnum(1);
        a1.setAvtag(true);
        a1.setTier("xwcxaxa1x");
        a1.setType(1);
        a1.setParent(new SysOrg("a"));
        deptList.add(a1);
        addPostionDept(deptList,"a1");

        SysOrgDept a2=new SysOrgDept();
        a2.setId("a2");
        a2.setName("厄瓜多尔");
        a2.setLabel("wc");
        a2.setOrnum(2);
        a2.setAvtag(true);
        a2.setTier("xwcxaxa2x");
        a2.setType(1);
        a2.setParent(new SysOrg("a"));
        deptList.add(a2);
        addPostionDept(deptList,"a2");

        SysOrgDept a3=new SysOrgDept();
        a3.setId("a3");
        a3.setName("塞内加尔");
        a3.setLabel("wc");
        a3.setOrnum(3);
        a3.setAvtag(true);
        a3.setTier("xwcxaxa3x");
        a3.setType(1);
        a3.setParent(new SysOrg("a"));
        deptList.add(a3);
        addPostionDept(deptList,"a3");

        SysOrgDept a4=new SysOrgDept();
        a4.setId("a4");
        a4.setName("荷兰");
        a4.setLabel("wc");
        a4.setOrnum(4);
        a4.setAvtag(true);
        a4.setTier("xwcxaxa4x");
        a4.setType(1);
        a4.setParent(new SysOrg("a"));
        deptList.add(a4);
        addPostionDept(deptList,"a4");

        SysOrgDept wcb=new SysOrgDept();
        wcb.setId("b");
        wcb.setName("B组_英伊美威");
        wcb.setLabel("wc");
        wcb.setOrnum(2);
        wcb.setAvtag(true);
        wcb.setTier("xwcxbx");
        wcb.setType(1);
        wcb.setParent(new SysOrg("wc"));
        deptList.add(wcb);

        SysOrgDept b1=new SysOrgDept();
        b1.setId("b1");
        b1.setName("英格兰");
        b1.setLabel("wc");
        b1.setOrnum(1);
        b1.setAvtag(true);
        b1.setTier("xwcxbxb1x");
        b1.setType(1);
        b1.setParent(new SysOrg("b"));
        deptList.add(b1);
        addPostionDept(deptList,"b1");

        SysOrgDept b2=new SysOrgDept();
        b2.setId("b2");
        b2.setName("伊朗");
        b2.setLabel("wc");
        b2.setOrnum(2);
        b2.setAvtag(true);
        b2.setTier("xwcxbxb2x");
        b2.setType(1);
        b2.setParent(new SysOrg("b"));
        deptList.add(b2);
        addPostionDept(deptList,"b2");

        SysOrgDept b3=new SysOrgDept();
        b3.setId("b3");
        b3.setName("美国");
        b3.setLabel("wc");
        b3.setOrnum(3);
        b3.setAvtag(true);
        b3.setTier("xwcxbxb3x");
        b3.setType(1);
        b3.setParent(new SysOrg("b"));
        deptList.add(b3);
        addPostionDept(deptList,"b3");

        SysOrgDept b4=new SysOrgDept();
        b4.setId("b4");
        b4.setName("威尔士");
        b4.setLabel("wc");
        b4.setOrnum(4);
        b4.setAvtag(true);
        b4.setTier("xwcxbxb4x");
        b4.setType(1);
        b4.setParent(new SysOrg("b"));
        deptList.add(b4);
        addPostionDept(deptList,"b4");

        SysOrgDept wcc=new SysOrgDept();
        wcc.setId("c");
        wcc.setName("C组_阿沙墨波");
        wcc.setLabel("wc");
        wcc.setOrnum(3);
        wcc.setAvtag(true);
        wcc.setTier("xwcxcx");
        wcc.setType(1);
        wcc.setParent(new SysOrg("wc"));
        deptList.add(wcc);

        SysOrgDept c1=new SysOrgDept();
        c1.setId("c1");
        c1.setName("阿根廷");
        c1.setLabel("wc");
        c1.setOrnum(1);
        c1.setAvtag(true);
        c1.setTier("xwcxcxc1x");
        c1.setType(1);
        c1.setParent(new SysOrg("c"));
        deptList.add(c1);
        addPostionDept(deptList,"c1");

        SysOrgDept c2=new SysOrgDept();
        c2.setId("c2");
        c2.setName("沙特");
        c2.setLabel("wc");
        c2.setOrnum(2);
        c2.setAvtag(true);
        c2.setTier("xwcxcxc2x");
        c2.setType(1);
        c2.setParent(new SysOrg("c"));
        deptList.add(c2);
        addPostionDept(deptList,"c2");

        SysOrgDept c3=new SysOrgDept();
        c3.setId("c3");
        c3.setName("墨西哥");
        c3.setLabel("wc");
        c3.setOrnum(3);
        c3.setAvtag(true);
        c3.setTier("xwcxcxc3x");
        c3.setType(1);
        c3.setParent(new SysOrg("c"));
        deptList.add(c3);
        addPostionDept(deptList,"c3");

        SysOrgDept c4=new SysOrgDept();
        c4.setId("c4");
        c4.setName("波兰");
        c4.setLabel("wc");
        c4.setOrnum(4);
        c4.setAvtag(true);
        c4.setTier("xwcxcxc4x");
        c4.setType(1);
        c4.setParent(new SysOrg("c"));
        deptList.add(c4);
        addPostionDept(deptList,"c4");

        SysOrgDept wcd=new SysOrgDept();
        wcd.setId("d");
        wcd.setName("D组_法澳丹突");
        wcd.setLabel("wc");
        wcd.setOrnum(4);
        wcd.setAvtag(true);
        wcd.setTier("xwcxdx");
        wcd.setType(1);
        wcd.setParent(new SysOrg("wc"));
        deptList.add(wcd);

        SysOrgDept d1=new SysOrgDept();
        d1.setId("d1");
        d1.setName("法国");
        d1.setLabel("wc");
        d1.setOrnum(1);
        d1.setAvtag(true);
        d1.setTier("xwcxdxd1x");
        d1.setType(1);
        d1.setParent(new SysOrg("d"));
        deptList.add(d1);
        addPostionDept(deptList,"d1");

        SysOrgDept d2=new SysOrgDept();
        d2.setId("d2");
        d2.setName("澳大利亚");
        d2.setLabel("wc");
        d2.setOrnum(2);
        d2.setAvtag(true);
        d2.setTier("xwcxdxd2x");
        d2.setType(1);
        d2.setParent(new SysOrg("d"));
        deptList.add(d2);
        addPostionDept(deptList,"d2");

        SysOrgDept d3=new SysOrgDept();
        d3.setId("d3");
        d3.setName("丹麦");
        d3.setLabel("wc");
        d3.setOrnum(3);
        d3.setAvtag(true);
        d3.setTier("xwcxdxd3x");
        d3.setType(1);
        d3.setParent(new SysOrg("d"));
        deptList.add(d3);
        addPostionDept(deptList,"d3");

        SysOrgDept d4=new SysOrgDept();
        d4.setId("d4");
        d4.setName("突尼斯");
        d4.setLabel("wc");
        d4.setOrnum(4);
        d4.setAvtag(true);
        d4.setTier("xwcxdxd4x");
        d4.setType(1);
        d4.setParent(new SysOrg("d"));
        deptList.add(d4);
        addPostionDept(deptList,"d4");

        SysOrgDept wce=new SysOrgDept();
        wce.setId("e");
        wce.setName("E组_西哥德日");
        wce.setLabel("wc");
        wce.setOrnum(5);
        wce.setAvtag(true);
        wce.setTier("xwcxex");
        wce.setType(1);
        wce.setParent(new SysOrg("wc"));
        deptList.add(wce);

        SysOrgDept e1=new SysOrgDept();
        e1.setId("e1");
        e1.setName("西班牙");
        e1.setLabel("wc");
        e1.setOrnum(1);
        e1.setAvtag(true);
        e1.setTier("xwcxexe1x");
        e1.setType(1);
        e1.setParent(new SysOrg("e"));
        deptList.add(e1);
        addPostionDept(deptList,"e1");

        SysOrgDept e2=new SysOrgDept();
        e2.setId("e2");
        e2.setName("哥斯达黎加");
        e2.setLabel("wc");
        e2.setOrnum(2);
        e2.setAvtag(true);
        e2.setTier("xwcxexe2x");
        e2.setType(1);
        e2.setParent(new SysOrg("e"));
        deptList.add(e2);
        addPostionDept(deptList,"e2");

        SysOrgDept e3=new SysOrgDept();
        e3.setId("e3");
        e3.setName("德国");
        e3.setLabel("wc");
        e3.setOrnum(3);
        e3.setAvtag(true);
        e3.setTier("xwcxexe3x");
        e3.setType(1);
        e3.setParent(new SysOrg("e"));
        deptList.add(e3);
        addPostionDept(deptList,"e3");

        SysOrgDept e4=new SysOrgDept();
        e4.setId("e4");
        e4.setName("日本");
        e4.setLabel("wc");
        e4.setOrnum(4);
        e4.setAvtag(true);
        e4.setTier("xwcxexe4x");
        e4.setType(1);
        e4.setParent(new SysOrg("e"));
        deptList.add(e4);
        addPostionDept(deptList,"e4");

        SysOrgDept wcf=new SysOrgDept();
        wcf.setId("f");
        wcf.setName("F组_比加摩克");
        wcf.setLabel("wc");
        wcf.setOrnum(6);
        wcf.setAvtag(true);
        wcf.setTier("xwcxfx");
        wcf.setType(1);
        wcf.setParent(new SysOrg("wc"));
        deptList.add(wcf);

        SysOrgDept f1=new SysOrgDept();
        f1.setId("f1");
        f1.setName("比利时");
        f1.setLabel("wc");
        f1.setOrnum(1);
        f1.setAvtag(true);
        f1.setTier("xwcxfxf1x");
        f1.setType(1);
        f1.setParent(new SysOrg("f"));
        deptList.add(f1);
        addPostionDept(deptList,"f1");

        SysOrgDept f2=new SysOrgDept();
        f2.setId("f2");
        f2.setName("加拿大");
        f2.setLabel("wc");
        f2.setOrnum(2);
        f2.setAvtag(true);
        f2.setTier("xwcxfxf2x");
        f2.setType(1);
        f2.setParent(new SysOrg("f"));
        deptList.add(f2);
        addPostionDept(deptList,"f2");

        SysOrgDept f3=new SysOrgDept();
        f3.setId("f3");
        f3.setName("摩洛哥");
        f3.setLabel("wc");
        f3.setOrnum(3);
        f3.setAvtag(true);
        f3.setTier("xwcxfxf3x");
        f3.setType(1);
        f3.setParent(new SysOrg("f"));
        deptList.add(f3);
        addPostionDept(deptList,"f3");

        SysOrgDept f4=new SysOrgDept();
        f4.setId("f4");
        f4.setName("克罗地亚");
        f4.setLabel("wc");
        f4.setOrnum(4);
        f4.setAvtag(true);
        f4.setTier("xwcxfxf4x");
        f4.setType(1);
        f4.setParent(new SysOrg("f"));
        deptList.add(f4);
        addPostionDept(deptList,"f4");

        SysOrgDept wcg=new SysOrgDept();
        wcg.setId("g");
        wcg.setName("G组_巴塞瑞喀");
        wcg.setLabel("wc");
        wcg.setOrnum(7);
        wcg.setAvtag(true);
        wcg.setTier("xwcxgx");
        wcg.setType(1);
        wcg.setParent(new SysOrg("wc"));
        deptList.add(wcg);

        SysOrgDept g1=new SysOrgDept();
        g1.setId("g1");
        g1.setName("巴西");
        g1.setLabel("wc");
        g1.setOrnum(1);
        g1.setAvtag(true);
        g1.setTier("xwcxgxg1x");
        g1.setType(1);
        g1.setParent(new SysOrg("g"));
        deptList.add(g1);
        addPostionDept(deptList,"g1");

        SysOrgDept g2=new SysOrgDept();
        g2.setId("g2");
        g2.setName("塞尔维亚");
        g2.setLabel("wc");
        g2.setOrnum(2);
        g2.setAvtag(true);
        g2.setTier("xwcxgxg2x");
        g2.setType(1);
        g2.setParent(new SysOrg("g"));
        deptList.add(g2);
        addPostionDept(deptList,"g2");

        SysOrgDept g3=new SysOrgDept();
        g3.setId("g3");
        g3.setName("瑞士");
        g3.setLabel("wc");
        g3.setOrnum(3);
        g3.setAvtag(true);
        g3.setTier("xwcxgxg3x");
        g3.setType(1);
        g3.setParent(new SysOrg("g"));
        deptList.add(g3);
        addPostionDept(deptList,"g3");

        SysOrgDept g4=new SysOrgDept();
        g4.setId("g4");
        g4.setName("喀麦隆");
        g4.setLabel("wc");
        g4.setOrnum(4);
        g4.setAvtag(true);
        g4.setTier("xwcxgxg4x");
        g4.setType(1);
        g4.setParent(new SysOrg("g"));
        deptList.add(g4);
        addPostionDept(deptList,"g4");

        SysOrgDept wch=new SysOrgDept();
        wch.setId("h");
        wch.setName("H组_葡加乌韩");
        wch.setLabel("wc");
        wch.setOrnum(8);
        wch.setAvtag(true);
        wch.setTier("xwcxhx");
        wch.setType(1);
        wch.setParent(new SysOrg("wc"));
        deptList.add(wch);

        SysOrgDept h1=new SysOrgDept();
        h1.setId("h1");
        h1.setName("葡萄牙");
        h1.setLabel("wc");
        h1.setOrnum(1);
        h1.setAvtag(true);
        h1.setTier("xwcxhxh1x");
        h1.setType(1);
        h1.setParent(new SysOrg("h"));
        deptList.add(h1);
        addPostionDept(deptList,"h1");

        SysOrgDept h2=new SysOrgDept();
        h2.setId("h2");
        h2.setName("加纳");
        h2.setLabel("wc");
        h2.setOrnum(2);
        h2.setAvtag(true);
        h2.setTier("xwcxhxh2x");
        h2.setType(1);
        h2.setParent(new SysOrg("h"));
        deptList.add(h2);
        addPostionDept(deptList,"h2");

        SysOrgDept h3=new SysOrgDept();
        h3.setId("h3");
        h3.setName("乌拉圭");
        h3.setLabel("wc");
        h3.setOrnum(3);
        h3.setAvtag(true);
        h3.setTier("xwcxhxh3x");
        h3.setType(1);
        h3.setParent(new SysOrg("h"));
        deptList.add(h3);
        addPostionDept(deptList,"h3");

        SysOrgDept h4=new SysOrgDept();
        h4.setId("h4");
        h4.setName("韩国");
        h4.setLabel("wc");
        h4.setOrnum(4);
        h4.setAvtag(true);
        h4.setTier("xwcxhxh4x");
        h4.setType(1);
        h4.setParent(new SysOrg("h"));
        deptList.add(h4);
        addPostionDept(deptList,"h4");

//        SysOrgDept ec1=new SysOrgDept();
//        ec1.setId("ec1");
//        ec1.setName("客户");
//        ec1.setLabel("ec1");
//        ec1.setOrnum(1);
//        ec1.setAvtag(true);
//        ec1.setTier("xec1x");
//        deptList.add(ec1);
//
//        SysOrgDept ec2=new SysOrgDept();
//        ec2.setId("ec2");
//        ec2.setName("渠道商");
//        ec2.setLabel("ec2");
//        ec2.setOrnum(2);
//        ec2.setAvtag(true);
//        ec2.setTier("xec2x");
//        deptList.add(ec2);
//
//        SysOrgDept ec3=new SysOrgDept();
//        ec3.setId("ec3");
//        ec3.setName("供应商");
//        ec3.setLabel("ec3");
//        ec3.setOrnum(3);
//        ec3.setAvtag(true);
//        ec3.setTier("xec3x");
//        deptList.add(ec3);

        deptService.insertAll(deptList);

    }

    //初始化管理员
    protected void initSa() {
        SysOrgUser user = new SysOrgUser();
        user.setId("sa");
        user.setUsnam("sa");
        user.setName("管理员");
        user.setPacod("1");
        user.setAvtag(true);
        user.setDept(new SysOrg("wc"));
        user.setTier("xwcxsax");
        userService.insert(user);

        SysOrgUser user2 = new SysOrgUser();
        user2.setId("vben");
        user2.setUsnam("vben");
        user2.setName("小维");
        user2.setPacod("123456");
        user2.setAvtag(true);
        user.setDept(new SysOrg("wc"));
        user2.setTier("xwcxvbenx");
        userService.insert(user2);
    }

    //初始化管理员
    protected void initTopUser() {
        List<SysOrgUser> userList=new ArrayList<>();
        SysOrgUser u1 = new SysOrgUser();
        u1.setId("l1");
        u1.setUsnam("l1");
        u1.setName("刘一");
        u1.setPacod("1");
        u1.setOrnum(1);
        u1.setAvtag(true);
        u1.setDept(new SysOrg("a"));
        u1.setTier("xwcxaxl1x");
        userList.add(u1);

        SysOrgUser u2 = new SysOrgUser();
        u2.setId("k2");
        u2.setUsnam("c2");
        u2.setName("陈二");
        u2.setPacod("1");
        u2.setOrnum(2);
        u2.setAvtag(true);
        u2.setDept(new SysOrg("b"));
        u2.setTier("xwcxbxk2x");
        userList.add(u2);

        SysOrgUser u3 = new SysOrgUser();
        u3.setId("z3");
        u3.setUsnam("z3");
        u3.setName("张三");
        u3.setPacod("1");
        u3.setOrnum(3);
        u3.setAvtag(true);
        u3.setDept(new SysOrg("c"));
        u3.setTier("xwcxcxz3x");
        userList.add(u3);

        SysOrgUser u4 = new SysOrgUser();
        u4.setId("l4");
        u4.setUsnam("l4");
        u4.setName("李四");
        u4.setPacod("1");
        u4.setOrnum(4);
        u4.setAvtag(true);
        u4.setDept(new SysOrg("d"));
        u4.setTier("xwcxdxl4x");
        userList.add(u4);

        SysOrgUser u5 = new SysOrgUser();
        u5.setId("w5");
        u5.setUsnam("w5");
        u5.setName("王五");
        u5.setPacod("1");
        u5.setOrnum(5);
        u5.setAvtag(true);
        u5.setDept(new SysOrg("e"));
        u5.setTier("xwcxexw5x");
        userList.add(u5);

        SysOrgUser u6 = new SysOrgUser();
        u6.setId("z6");
        u6.setUsnam("z6");
        u6.setName("赵六");
        u6.setPacod("1");
        u6.setOrnum(6);
        u6.setAvtag(true);
        u6.setDept(new SysOrg("f"));
        u6.setTier("xwcxfxz6x");
        userList.add(u6);

        SysOrgUser u7 = new SysOrgUser();
        u7.setId("s7");
        u7.setUsnam("s7");
        u7.setName("孙七");
        u7.setPacod("1");
        u7.setOrnum(7);
        u7.setAvtag(true);
        u7.setDept(new SysOrg("g"));
        u7.setTier("xwcxgxs7x");
        userList.add(u7);

        SysOrgUser u8 = new SysOrgUser();
        u8.setId("z8");
        u8.setUsnam("z8");
        u8.setName("周八");
        u8.setPacod("1");
        u8.setOrnum(8);
        u8.setAvtag(true);
        u8.setDept(new SysOrg("h"));
        u8.setTier("xwcxhxz8x");
        userList.add(u8);

        userService.insertAll(userList);
    }


    private void addPostionDept(List<SysOrgDept> deptList,String id){
//        SysOrgDept qf=new SysOrgDept();
//        qf.setId(id+"qf");
//        qf.setName(XstrUtil.toUpperfirst(id)+"_前锋");
//        qf.setLabel("wc");
//        qf.setOrnum(1);
//        qf.setAvtag(true);
//        qf.setTier("xwcx"+id.substring(0,1)+"x"+id+"x"+id+"qf"+"x");
//        qf.setType(2);
//        qf.setParent(new SysOrg(id));
//        deptList.add(qf);
//
//        SysOrgDept zc=new SysOrgDept();
//        zc.setId(id+"zc");
//        zc.setName(XstrUtil.toUpperfirst(id)+"_中场");
//        zc.setLabel("wc");
//        zc.setOrnum(2);
//        zc.setAvtag(true);
//        zc.setTier("xwcx"+id.substring(0,1)+"x"+id+"x"+id+"zc"+"x");
//        zc.setType(2);
//        zc.setParent(new SysOrg(id));
//        deptList.add(zc);
//
//        SysOrgDept hw=new SysOrgDept();
//        hw.setId(id+"hw");
//        hw.setName(XstrUtil.toUpperfirst(id)+"_后卫");
//        hw.setLabel("wc");
//        hw.setOrnum(3);
//        hw.setAvtag(true);
//        hw.setTier("xwcx"+id.substring(0,1)+"x"+id+"x"+id+"hw"+"x");
//        hw.setType(2);
//        hw.setParent(new SysOrg(id));
//        deptList.add(hw);
//
//        SysOrgDept mj=new SysOrgDept();
//        mj.setId(id+"mj");
//        mj.setName(XstrUtil.toUpperfirst(id)+"_门将");
//        mj.setLabel("wc");
//        mj.setOrnum(4);
//        mj.setAvtag(true);
//        mj.setTier("xwcx"+id.substring(0,1)+"x"+id+"x"+id+"mj"+"x");
//        mj.setType(2);
//        mj.setParent(new SysOrg(id));
//        deptList.add(mj);

    }


    protected void initGroup(){
        SysOrgGroup group=new SysOrgGroup();
        group.setId("z3l4");
        group.setName("张三李四组");
        group.setAvtag(true);
        List<SysOrg> list=new ArrayList<>();
        list.add(new SysOrg("l1"));
        list.add(new SysOrg("k2"));
        list.add(new SysOrg("z3"));
        list.add(new SysOrg("l4"));
        list.add(new SysOrg("w5"));
        list.add(new SysOrg("z6"));
        list.add(new SysOrg("s7"));
        list.add(new SysOrg("z8"));
        group.setMembers(list);
        groupService.insert(group);

    }

    @Autowired
    private SysOrgGroupService groupService;

    @Autowired
    private  SysOrgDeptService deptService;

    @Autowired
    private  SysOrgUserService userService;

}
